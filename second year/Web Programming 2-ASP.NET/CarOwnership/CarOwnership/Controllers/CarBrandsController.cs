﻿using CarOwnership.Entities;
using CarOwnership.Filters;
using CarOwnership.Repositories;
using CarOwnership.ViewModels.CarBrands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CarOwnership.Controllers
{
    [AuthenticationFilter]
    public class CarBrandsController : Controller
    {
        CarBrandRepository repo = new CarBrandRepository();

        public ActionResult Index()
        {
            IndexVM model = new IndexVM();
            model.CarBrands = repo.GetAll();
            return View(model);
        }

        [HttpGet]
        public ActionResult Edit(int? id)
        {
            CarBrand item = id == null ? new CarBrand() : repo.GetById(id.Value);

            EditVM model = new EditVM();
            model.Country = item.Country;
            model.Name = item.Name;
            model.Id = item.Id;

            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(EditVM model)
        {
            if (!ModelState.IsValid)
                return View(model);

            CarBrand item = new CarBrand();
            item.Id = model.Id;
            item.Country = model.Country;
            item.Name = model.Name;

            if (item.Id == 0)
            {
                repo.Insert(item);
            }
            else
            {
                repo.Update(item);
            }

            return RedirectToAction("Index", "CarBrands");
        }

        public ActionResult Delete(int id)
        {     
            repo.Delete(id);

            return RedirectToAction("Index", "CarBrands");
        }
    }
}
