﻿using CarOwnership.Entities;
using CarOwnership.Filters;
using CarOwnership.Repositories;
using CarOwnership.ViewModels.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CarOwnership.Controllers
{
    [AuthenticationFilter]
    public class UsersController : Controller
    {
        UserRepository repo = new UserRepository();

        public ActionResult Index()
        {
            User loggedUser = ((User)Session["loggedUser"]);
            if (loggedUser.Username != "admin" && loggedUser.Password !="admin")
            {
                return RedirectToAction("Index", "Home");
            }
            IndexVM model = new IndexVM();
            model.Users = repo.GetAll();
            return View(model);
        }

        [HttpGet]
        public ActionResult Edit(int? id)
        {
            User item = id == null ? new User() : repo.GetById(id.Value);

            EditVM model = new EditVM();
            model.Username = item.Username;
            model.Password = item.Password;
            model.Id = item.Id;
            model.FirstName = item.FirstName;
            model.LastName = item.LastName;

            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(EditVM model)
        {
            if (!ModelState.IsValid)
                return View(model);

            User item = new User();
            item.Id = model.Id;
            item.Username = model.Username;
            item.Password = model.Password;
            item.FirstName = model.FirstName;
            item.LastName = model.LastName;

            if (item.Id == 0)
            {
                repo.Insert(item);
            }
            else
            {
                repo.Update(item);
            }

            return RedirectToAction("Index", "Users");
        }

        public ActionResult Delete(int id)
        {
            repo.Delete(id);

            return RedirectToAction("Index", "Users");
        }
    }
}
