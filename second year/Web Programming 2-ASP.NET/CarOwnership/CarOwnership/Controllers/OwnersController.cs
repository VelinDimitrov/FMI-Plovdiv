﻿using CarOwnership.Entities;
using CarOwnership.Filters;
using CarOwnership.Repositories;
using CarOwnership.ViewModels.Owners;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CarOwnership.Controllers
{
    [AuthenticationFilter]
    public class OwnersController : Controller
    {
        OwnerRepository repo = new OwnerRepository();

        public ActionResult Index()
        {
            IndexVM model = new IndexVM();
            model.Owners = repo.GetAll();
            return View(model);
        }

        [HttpGet]
        public ActionResult Edit(int? id)
        {
            Owner item = id == null ? new Owner() : repo.GetById(id.Value);

            EditVM model = new EditVM();
            model.Email = item.Email;           
            model.Id = item.Id;
            model.FirstName = item.FirstName;
            model.LastName = item.LastName;

            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(EditVM model)
        {
            if (!ModelState.IsValid)
                return View(model);

            Owner item = new Owner();
            item.Id = model.Id;
            item.Email = model.Email;            
            item.FirstName = model.FirstName;
            item.LastName = model.LastName;

            if (item.Id == 0)
            {
                repo.Insert(item);
            }
            else
            {
                repo.Update(item);
            }

            return RedirectToAction("Index", "Owners");
        }

        public ActionResult Delete(int id)
        {
            repo.Delete(id);

            return RedirectToAction("Index", "Owners");
        }
    }
}
