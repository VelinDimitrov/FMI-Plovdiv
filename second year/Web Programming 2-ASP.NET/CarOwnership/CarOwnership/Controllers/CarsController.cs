﻿using CarOwnership.Entities;
using CarOwnership.Filters;
using CarOwnership.Repositories;
using CarOwnership.ViewModels.Cars;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CarOwnership.Controllers
{
    [AuthenticationFilter]
    public class CarsController : Controller
    {
        CarRepository repo = new CarRepository();
        CarBrandRepository brandsRepo = new CarBrandRepository();        

        public ActionResult Index(int ownerId,int? partId)
        {
            IndexVM model = new IndexVM();
            model.OwnerId = ownerId;            
            model.Cars = repo.GetCarsByOwnerId(ownerId);
            return View(model);          
        }

        [HttpGet]
        public ActionResult Edit(int? id,int ownerId)
        {
            Car item = id == null ? new Car() : repo.GetById(id.Value);

            EditVM model = new EditVM();
            model.FuelTankVolume = item.FuelTankVolume;
            model.FuelType = item.FuelType;
            model.Id = item.Id;
            model.Model = item.Model;
            model.Power = item.Power;            
            model.OwnerId = ownerId;
            model.CarBrandId = item.CarBrandId;           
            ViewBag.Brands = brandsRepo.GetAll();

            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(EditVM form)
        {
            if (!ModelState.IsValid)
                return View(form);

            Car item = new Car();
            item.Id = form.Id;
            item.FuelTankVolume = form.FuelTankVolume;
            item.FuelType = form.FuelType;           
            item.Model = form.Model;
            item.Power = form.Power;
            item.OwnerId = form.OwnerId;
            item.CarBrandId = form.CarBrandId;

            if (item.Id == 0)
            {
                repo.Insert(item);
            }
            else
            {
                repo.Update(item);
            }

            return RedirectToAction("Index", "Cars", new { ownerId = item.OwnerId });
        }

        public ActionResult Delete(int id,int ownerId)
        {
            repo.Delete(id);

            return RedirectToAction("Index", "Cars", new { ownerId });
        }

    }
}
