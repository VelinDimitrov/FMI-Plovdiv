﻿using CarOwnership.Entities;
using CarOwnership.Repositories;
using CarOwnership.ViewModels.Home;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CarOwnership.Controllers
{
    public class HomeController : Controller
    {
        UserRepository repo = new UserRepository();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Login()
        {            
            return View(new LoginVM());
        }

        [HttpPost]
        public ActionResult Login(LoginVM model)
        {
            if (!ModelState.IsValid)
                return View(model);

            User user = repo.GetByUsernamePassword(model.Username, model.Password);

            Session["loggedUser"] = user;

            if (Session["loggedUser"] == null)
                ModelState.AddModelError("AuthenticationFailed", "Authentication failed!");

            if (!ModelState.IsValid)
                return View(model);

            return RedirectToAction("Index", "Home");            
        }

        public ActionResult Logout()
        {
            Session["loggedUser"] = null;            

            return RedirectToAction("Login", "Home");
        }
    }
}