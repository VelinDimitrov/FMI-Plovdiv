﻿using CarOwnership.Entities;
using CarOwnership.Filters;
using CarOwnership.Repositories;
using CarOwnership.ViewModels.Parts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CarOwnership.Controllers
{
    [AuthenticationFilter]
    public class PartsController : Controller
    {
        private static CarOwnershipDbContext context = new CarOwnershipDbContext();
        PartRepository repo = new PartRepository(context);
        CarRepository carRepo = new CarRepository(context);

        public ActionResult Index(int? carId)
        {
            IndexVM model = new IndexVM();
            if (carId != null)
            {
                model.CarId = carId.Value;
                model.Parts = carRepo.GetById(carId.Value).PartsInCar.ToList();
                return View(model);
            }
            model.Parts = repo.GetAll();

            return View(model);
        }

        public ActionResult Details(int id)
        {
            DetailsVM model = new DetailsVM();
            model.Part = repo.GetById(id);
            model.Cars = model.Part.CarsContainingPart.ToList();
            
            return View(model);
        }

        [HttpGet]
        public ActionResult Edit(int? id)
        {
            Part item = id == null ? new Part() : repo.GetById(id.Value);

            EditVM model = new EditVM();
            model.Id = item.Id;
            model.Name = item.Name;
            model.CountryProduced = item.CountryProduced;
            model.Producer = item.Producer;
            ViewBag.Cars = carRepo.GetAll();

            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(EditVM form)
        {
            if (!ModelState.IsValid)
                return View(form);            

            Part item = form.Id == 0 ? new Part() : repo.GetById(form.Id);
            item.Id = form.Id;
            item.Name = form.Name;
            item.CountryProduced = form.CountryProduced;
            item.Producer = form.Producer;
            item.CarsContainingPart = carRepo.GetAll(c => form.CarIds.Contains(c.Id)); ;           

            if (item.Id == 0)
            {
                repo.Insert(item);
            }
            else
            {
                repo.Update(item);
            }

            return RedirectToAction("Index", "Parts");
        }

        public ActionResult Delete(int id)
        {
            repo.Delete(id);

            return RedirectToAction("Index", "Parts");
        }
    }
}
