﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CarOwnership.Entities
{
    public class Car : BaseEntity
    {
        public int CarBrandId { get; set; }
        public int OwnerId { get; set; }
        public string Model { get; set; }
        public string FuelType { get; set; }
        public string Power { get; set; }
        public string FuelTankVolume { get; set; }

        [ForeignKey("CarBrandId")]
        public virtual CarBrand Brand { get; set; }

        [ForeignKey("OwnerId")]
        public Owner CarOwner { get; set; }

        public virtual ICollection<Part> PartsInCar { get; set; }
    }
}