﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarOwnership.Entities
{
    public class Owner : BaseEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }

        public virtual ICollection<Car> OwnedCars { get; set; }
    }
}