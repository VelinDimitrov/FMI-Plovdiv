﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarOwnership.Entities
{
    public class CarBrand : BaseEntity
    {
        public string Name { get; set; }
        public string Country { get; set; }
    }
}