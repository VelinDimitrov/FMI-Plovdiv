﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarOwnership.Entities
{
    public class Part : BaseEntity
    {
        public string Name { get; set; }
        public string Producer { get; set; }
        public string CountryProduced { get; set; }

        public virtual ICollection<Car> CarsContainingPart { get; set; }
    }
}