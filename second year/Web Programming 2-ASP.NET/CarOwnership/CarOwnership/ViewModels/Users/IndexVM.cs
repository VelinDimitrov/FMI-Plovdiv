﻿using CarOwnership.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarOwnership.ViewModels.Users
{
    public class IndexVM
    {
        public List<User> Users { get; set; }
    }
}