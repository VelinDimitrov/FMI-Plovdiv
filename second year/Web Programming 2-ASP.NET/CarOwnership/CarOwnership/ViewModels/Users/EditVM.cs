﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CarOwnership.ViewModels.Users
{
    public class EditVM
    {      
        public int Id { get; set; }

        [Required(ErrorMessage = "This field is Required!")]
        public string Username { get; set; }

        [Required(ErrorMessage = "This field is Required!")]
        public string Password { get; set; }

        [Required(ErrorMessage = "This field is Required!")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "This field is Required!")]
        public string LastName { get; set; }
    }
}