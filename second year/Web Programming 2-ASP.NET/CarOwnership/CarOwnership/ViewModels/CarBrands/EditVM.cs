﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CarOwnership.ViewModels.CarBrands
{
    public class EditVM
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "This field is Required!")]
        public string Name { get; set; }

        [Required(ErrorMessage = "This field is Required!")]
        public string Country { get; set; }
    }
}