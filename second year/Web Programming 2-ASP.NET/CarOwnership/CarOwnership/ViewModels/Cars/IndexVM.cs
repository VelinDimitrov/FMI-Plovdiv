﻿using CarOwnership.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarOwnership.ViewModels.Cars
{
    public class IndexVM
    {
        public List<Car> Cars { get; set; }
        public int OwnerId { get; set; }
    }
}