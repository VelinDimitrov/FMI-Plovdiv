﻿using CarOwnership.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CarOwnership.ViewModels.Cars
{
    public class EditVM
    {
        public int Id { get; set; }
        public int CarBrandId { get; set; }        
        public int OwnerId { get; set; }

        [Required(ErrorMessage = "This field is Required!")]
        public string Model { get; set; }

        [Required(ErrorMessage = "This field is Required!")]
        public string FuelType { get; set; }

        [Required(ErrorMessage = "This field is Required!")]
        public string Power { get; set; }

        [Required(ErrorMessage = "This field is Required!")]
        public string FuelTankVolume { get; set; }        
    }
}