﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarOwnership.ViewModels.Home
{
    public class LoginVM
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}