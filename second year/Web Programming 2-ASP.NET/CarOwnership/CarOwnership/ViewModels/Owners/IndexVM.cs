﻿using CarOwnership.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarOwnership.ViewModels.Owners
{
    public class IndexVM
    {    
        public List<Owner> Owners { get; set; }
    }
}