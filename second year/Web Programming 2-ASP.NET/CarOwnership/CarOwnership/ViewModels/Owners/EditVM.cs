﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CarOwnership.ViewModels.Owners
{
    public class EditVM
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "This field is Required!")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "This field is Required!")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "This field is Required!")]
        public string Email { get; set; }
    }
}