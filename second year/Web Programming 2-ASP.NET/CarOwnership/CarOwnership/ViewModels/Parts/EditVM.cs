﻿using CarOwnership.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CarOwnership.ViewModels.Parts
{
    public class EditVM
    {       
        public int Id { get; set; }

        [Required(ErrorMessage = "This field is Required!")]
        public string Name { get; set; }

        [Required(ErrorMessage = "This field is Required!")]
        public string Producer { get; set; }

        [Required(ErrorMessage = "This field is Required!")]
        public string CountryProduced { get; set; }

        public List<int> CarIds { get; set; }
    }
}