﻿using CarOwnership.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarOwnership.ViewModels.Parts
{
    public class DetailsVM
    {
        public Part Part { get; set; }
        public List<Car> Cars { get; set; }
    }
}