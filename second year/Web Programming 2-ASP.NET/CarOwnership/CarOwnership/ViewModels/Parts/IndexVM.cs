﻿using CarOwnership.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarOwnership.ViewModels.Parts
{
    public class IndexVM
    {
        public List<Part> Parts { get; set; }
        public int CarId { get; set; }
    }
}