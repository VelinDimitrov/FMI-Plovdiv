﻿using CarOwnership.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace CarOwnership.Repositories
{
    public class PartRepository : BaseRepository<Part>
    {
        public PartRepository() : base(new CarOwnershipDbContext())
        {
        }
        public PartRepository(CarOwnershipDbContext context) : base(context)
        {
        }        
    }
}