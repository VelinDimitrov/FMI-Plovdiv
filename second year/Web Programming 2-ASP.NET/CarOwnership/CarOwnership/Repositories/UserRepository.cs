﻿using CarOwnership.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace CarOwnership.Repositories
{
    public class UserRepository : BaseRepository<User>
    {
        public UserRepository() : base(new CarOwnershipDbContext())
        {
        }

        public User GetByUsernamePassword(string username, string password) {
            return context
                .Set<User>()
                .Where(u => u.Username == username && u.Password == password)
                .FirstOrDefault();
        }
    }
}