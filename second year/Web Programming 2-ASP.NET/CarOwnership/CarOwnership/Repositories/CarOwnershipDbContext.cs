﻿using CarOwnership.Entities;
using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarOwnership.Repositories
{
    public class CarOwnershipDbContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Car> Cars { get; set; }
        public DbSet<CarBrand> CarBrands { get; set; }
        public DbSet<Part> Parts { get; set; }
        public DbSet<Owner> Owners { get; set; }

        public CarOwnershipDbContext()
            : base(@"Server=localhost;Database=CarOwnershipDB;Trusted_Connection=True;")
        {
            if (!Database.Exists())
            {
                Database.SetInitializer(new CarOwnershipDatabaseInitializer());
                Database.Initialize(true);
            }
            Users = Set<User>();
            Cars = Set<Car>();
            CarBrands = Set<CarBrand>();
            Parts = Set<Part>();
            Owners = Set<Owner>();          
        }

        public System.Data.Entity.DbSet<CarOwnership.ViewModels.Users.EditVM> EditVMs { get; set; }
    }
}