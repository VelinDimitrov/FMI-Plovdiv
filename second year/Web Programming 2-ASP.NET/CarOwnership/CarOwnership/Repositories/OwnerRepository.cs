﻿using CarOwnership.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace CarOwnership.Repositories
{
    public class OwnerRepository : BaseRepository<Owner>
    {
        public OwnerRepository() : base(new CarOwnershipDbContext())
        {
        }
    }
}