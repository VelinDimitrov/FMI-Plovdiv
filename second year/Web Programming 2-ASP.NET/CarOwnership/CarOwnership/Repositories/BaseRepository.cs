﻿using System;
using CarOwnership.Entities;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace CarOwnership.Repositories
{
    public class BaseRepository<E> where E : BaseEntity
    {
        protected DbContext context { get; set; }

        public BaseRepository(DbContext DBContext)
        {
            context = DBContext;
        }
        
		public E GetById(int id)
		{            
			return context.Set<E>().Find(id);
		}

        public List<E> GetAll()
        {
            return context.Set<E>().ToList();
        }

        public List<E> GetAll(Expression<Func<E, bool>> filter)
        {
            return context.Set<E>().Where(filter).ToList();
        }

        public void Insert(E item)
        {
            context.Entry(item).State = EntityState.Added;
            context.SaveChanges();
        }

        public void Update(E item)
        {
            context.Entry(item).State = EntityState.Modified;           
        }
        
		public void Delete(int id)
		{
			context.Entry(GetById(id)).State = EntityState.Deleted;
			context.SaveChanges();
		}
    }
}