﻿using CarOwnership.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace CarOwnership.Repositories
{
    public class CarBrandRepository : BaseRepository<CarBrand>
    {
        public CarBrandRepository() : base(new CarOwnershipDbContext())
        {
        }        
    }
}