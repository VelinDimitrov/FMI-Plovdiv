﻿using CarOwnership.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace CarOwnership.Repositories
{
    public class CarOwnershipDatabaseInitializer : DropCreateDatabaseAlways<CarOwnershipDbContext>
    {
        protected override void Seed(CarOwnershipDbContext context)
        {
            context.Users.Add(new User {
                FirstName = "Velin",
                LastName = "Dimitrov",
                Username = "admin",
                Password = "admin"
            });
            context.Users.Add(new User
            {
                FirstName = "Test",
                LastName = "Dimitrov",
                Username = "user",
                Password = "user"
            });
            context.CarBrands.Add(new CarBrand { Id=1, Name = "Peugeot",Country = "France"});
            context.Owners.Add(new Owner{
                FirstName = "Velin",
                LastName = "Dimitrov",
                Email = "test@abv.bg",
                OwnedCars = new List<Car> {
                    new Car {
                        CarBrandId = 1,
                        FuelTankVolume = "50",
                        FuelType = "Gasoline",
                        Model = "206 1.1i",
                        Power = "60hp",
                        PartsInCar = new List<Part>{
                            new Part{
                                Name = "CrankShaft Sensor",
                                Producer = "Bosch",
                                CountryProduced = "Germany",                                
                            }
                        }
                    }
                }
            });
            base.Seed(context);
        }
    }
}