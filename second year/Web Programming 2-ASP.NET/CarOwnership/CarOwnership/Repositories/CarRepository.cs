﻿using CarOwnership.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace CarOwnership.Repositories
{
    public class CarRepository : BaseRepository<Car>
    {
        public CarRepository() : base(new CarOwnershipDbContext())
        {
        }
        public CarRepository(CarOwnershipDbContext context) : base(context)
        {
        }

        public List<Car> GetCarsByOwnerId(int ownerId) {
            return context.Set<Car>().Where(c => c.OwnerId == ownerId).ToList();
        }
    }
}