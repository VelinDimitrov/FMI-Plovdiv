﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UsersManager.Entities;

namespace UsersManager.ViewModels.Phones
{
    public class IndexVM
    {
        public int ParentContactId { get; set; }

        public Contact Contact { get; set; }
        public List<Phone> Items { get; set; }
    }
}