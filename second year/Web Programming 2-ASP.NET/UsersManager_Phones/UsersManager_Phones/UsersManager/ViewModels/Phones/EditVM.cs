﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace UsersManager.ViewModels.Phones
{
	public class EditVM
	{
		public int Id { get; set; }		

		public int ContactId { get; set; }

		[DisplayName("Number: ")]
		[Required(ErrorMessage = "This field is Required!")]
		public string Number { get; set; }
	}
}