﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UsersManager.Entities;

namespace UsersManager.ViewModels.Contacts
{
    public class IndexVM
    {
        public List<Contact> Items { get; set; }
    }
}