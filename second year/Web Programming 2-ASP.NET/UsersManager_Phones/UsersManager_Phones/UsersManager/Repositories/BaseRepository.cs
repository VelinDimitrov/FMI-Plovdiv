﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace UsersManager.Repositories
{
	public class BaseRepository<E> where E : class
	{
		private DbContext context { get; set; }

		public BaseRepository()
		{
			context = new UsersManagerDbContext();
		}
        /*
		public E GetById(int id)
		{
			return context.E.Find(id);
		}

		public List<E> GetAll(int parentContactId)
		{
			return context.E.Where(p => p.ContactId == parentContactId).ToList();
		}*/

		public void Insert(E item)
		{
			context.Entry(item).State = EntityState.Added;
			context.SaveChanges();
		}

		public void Update(E item)
		{
			context.Entry(item).State = EntityState.Modified;
			context.SaveChanges();
		}
        /*
		public void Delete(int id)
		{
			context.Entry(GetById(id)).State = EntityState.Deleted;
			context.SaveChanges();
		}*/
	}
}