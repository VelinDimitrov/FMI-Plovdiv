﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UsersManager.Entities;

namespace UsersManager.Repositories
{
    public class UsersRepository
    {
        public User GetById(int id)
        {
            UsersManagerDbContext context = new UsersManagerDbContext();

            return context.Users.Where(i => i.Id == id)
                                .FirstOrDefault();
        }

        public List<User> GetAll()
        {
            UsersManagerDbContext context = new UsersManagerDbContext();

            return context.Users.ToList();
        }

        public void Insert(User item)
        {
            UsersManagerDbContext context = new UsersManagerDbContext();

            context.Users.Add(item);

            context.SaveChanges();
        }

        public void Update(User item)
        {
            UsersManagerDbContext context = new UsersManagerDbContext();

            context.Entry(item).State = EntityState.Modified;

            context.SaveChanges();
        }

        public void Delete(int id)
        {
            UsersManagerDbContext context = new UsersManagerDbContext();
            User item = context.Users.Where(i => i.Id == id)
                                     .FirstOrDefault();
            context.Users.Remove(item);

            context.SaveChanges();
        }

        public User GetByUsernameAndPassword(string username, string password)
        {
            UsersManagerDbContext context = new UsersManagerDbContext();

            return context.Users.Where(i => i.Username == username &&
                                            i.Password == password)
                                .FirstOrDefault();
        }
    }
}