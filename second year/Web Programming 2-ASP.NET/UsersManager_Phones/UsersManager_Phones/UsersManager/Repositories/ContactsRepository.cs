﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UsersManager.Entities;

namespace UsersManager.Repositories
{
    public class ContactsRepository
    {
        public Contact GetById(int id)
        {
            UsersManagerDbContext context = new UsersManagerDbContext();

            return context.Contacts.Where(i => i.Id == id)
                                .FirstOrDefault();
        }

        public List<Contact> GetAll(int parentUserId)
        {
            UsersManagerDbContext context = new UsersManagerDbContext();

            return context.Contacts.Where(i => i.UserId == parentUserId)
                                   .ToList();
        }

        public void Insert(Contact item)
        {
            UsersManagerDbContext context = new UsersManagerDbContext();

            context.Contacts.Add(item);

            context.SaveChanges();
        }

        public void Update(Contact item)
        {
            UsersManagerDbContext context = new UsersManagerDbContext();

            context.Entry(item).State = EntityState.Modified;

            context.SaveChanges();
        }

        public void Delete(int id)
        {
            UsersManagerDbContext context = new UsersManagerDbContext();
            Contact item = context.Contacts.Where(i => i.Id == id)
                                     .FirstOrDefault();
            context.Contacts.Remove(item);

            context.SaveChanges();
        }

    }
}