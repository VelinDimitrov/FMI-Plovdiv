﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using UsersManager.Entities;

namespace UsersManager.Repositories
{
    public class UsersManagerDbContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Contact> Contacts { get; set; }
        public DbSet<Phone> Phones { get; set; }

        public UsersManagerDbContext()
            : base(@"Server=localhost;Database=UserManagerDB;Trusted_Connection=True;")
        {
            Users = Set<User>();
            Contacts = Set<Contact>();
            Phones = Set<Phone>();
        }

    }
}