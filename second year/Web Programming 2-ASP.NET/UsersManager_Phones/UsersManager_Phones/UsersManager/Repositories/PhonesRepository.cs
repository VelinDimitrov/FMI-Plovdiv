﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UsersManager.Entities;

namespace UsersManager.Repositories
{
    public class PhonesRepository
    {
		UsersManagerDbContext context = new UsersManagerDbContext();

		public Phone GetById(int id)
        {			
			return context.Phones.Find(id);
        }

        public List<Phone> GetAll(int parentContactId)
        {
			return context.Phones.Where(p =>p.ContactId == parentContactId).ToList();
        }

        public void Insert(Phone item)
        {
			context.Entry(item).State = EntityState.Added;
			context.SaveChanges();
        }

        public void Update(Phone item)
        {			
			context.Entry(item).State = EntityState.Modified;
			context.SaveChanges();
		}

        public void Delete(int id)
        {
			context.Entry(GetById(id)).State = EntityState.Deleted;
			context.SaveChanges();
		}

    }
}