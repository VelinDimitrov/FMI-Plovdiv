﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UsersManager.Entities;
using UsersManager.Filters;
using UsersManager.Repositories;
using UsersManager.ViewModels.Phones;

namespace UsersManager.Controllers
{
	[AuthenticationFilter]
	public class PhonesController : Controller
    {
        public ActionResult Index(IndexVM model)
        {			
			PhonesRepository phonesRepo = new PhonesRepository();
            ContactsRepository repo = new ContactsRepository();

            model.Contact = repo.GetById(model.ParentContactId);
            model.Items = phonesRepo.GetAll(model.ParentContactId);
            
            return View(model);
        }

		[HttpGet]
		public ActionResult Edit(int contactId,int? phoneId)
		{
			PhonesRepository phonesRepo = new PhonesRepository();
			ContactsRepository repo = new ContactsRepository();

			Phone item = phoneId == null ? new Phone() : phonesRepo.GetById(phoneId.Value);

			EditVM model = new EditVM();
			model.Id = item.Id;
			model.ContactId = contactId;
			model.Number = item.Number;

			return View(model);			
		}

		[HttpPost]
		public ActionResult Edit(EditVM model)
		{
			if (!ModelState.IsValid)
				return View(model);

			PhonesRepository phonesRepo = new PhonesRepository();			

			Phone item = new Phone();
			item.Id = model.Id;
			item.ContactId = model.ContactId;
			item.Number = model.Number;

			if (item.Id == 0)
			{
				phonesRepo.Insert(item);
			}
			else
			{
				phonesRepo.Update(item);
			}
			
			return RedirectToAction("Index", "Phones", new { ParentContactId = item.ContactId });
		}

		public ActionResult Delete(int id,int contactId)
		{
			PhonesRepository repo = new PhonesRepository();

			repo.Delete(id);

			return RedirectToAction("Index", "Phones", new { ParentContactId = contactId });
		}
	}
}