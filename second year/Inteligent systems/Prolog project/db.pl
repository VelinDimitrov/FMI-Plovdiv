bird(owl).
bird(eagel).
bird(gosho).
animal(cat).
animal(mouse).
animal(lion).
fish(shark).
fish(dolphin).
fish(kosatka).

isSameType(X, Y) :-
	bird(X) , bird(Y) ;
	animal(X) , animal(Y) ;
	fish(X) , fish(Y).
	
excerciseThree(L) :-
	foreach(member(X, L), bird(X)) ;
	foreach(member(X, L), animal(X)) ;
	foreach(member(X, L), fish(X)) .
	
eat(X,Y) :-
	bird(X) , animal(Y) ;
	fish(X) , bird(Y) ;
	animal(X) , fish(Y) .
	
excerciseFive(L,X) :-
	member(X,L).
	
excerciseSeven([X|T]) :-
	member(X,T) ; 
	excerciseSeven(T).
