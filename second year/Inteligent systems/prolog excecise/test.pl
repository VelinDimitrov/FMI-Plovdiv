loves(romeo, juliet).

loves(juliet, romeo) :- loves(romeo, juliet).

male(albert).
male(bob).
male(bill).
male(carl).
male(charlie).
male(gosho).
male(pesho).

female(alice).
female(betsy).
female(diana).

happy(albert).
happy(alice).
happy(bob).
happy(bill).
with_albert(alice).

runs(X) :- happy(X).

dances(alice) :- happy(alice) , with_albert(alice).

does_alice_dance :- dances(alice),
write('Alice dances when happy with albert').

parent(albert, bob).
parent(albert, betsy).
parent(albert, bill).

parent(alice, bob).
parent(alice, betsy).
parent(alice, bill).

parent(bob,carl).
parent(bob, charlie).

get_grandchild(X) :-
	parent(X, Y),
	parent(Y,Z),
	write(X),
	write(' has grandchild '),
	write(Z).
	
get_grandparent(X) :-
	parent(Y, X),
	parent(Z,Y),
	format('~w ~s grandparent of ~w',[X,"is the",Z]).