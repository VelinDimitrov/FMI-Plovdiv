package com.fmi.project.model;
import java.util.ArrayList;

public class SecondExcerciseSearch implements Searchable{
	
	Graph myMap;
	
	public SecondExcerciseSearch(Graph g) {
		this.myMap = g;
	}
	
	public boolean search(String startName, String endName) {
		//TODO create depth variable for shortest way and change when way found
		
		if(!myMap.containsNode(startName) || !myMap.containsNode(endName)) {
			return false;
		}
		
		Room startNode = myMap.getRoom(startName);
		ArrayList<Room> list = new ArrayList<>();
		list.add(startNode);
		startNode.depth = 0;
		
		Room temp;
		
		while(!list.isEmpty()) {
			temp = list.get(0);
			System.out.println("Current room is: " + temp.getRoomName() 
					+ " and depth is: " + temp.depth);
			
			if(temp.getRoomName().equals(endName)) {
				myMap.printPath(temp);
				return true;
			}
			myMap.setDepths(temp.getRoomName());
			temp.isTested = true;
			list.remove(0);
			
			for(Transition trans : temp.getTransitions()) {
				Room node = trans.getToRoom();
				if(!node.isTested && !list.contains(node)) {
					node.distanceToGoal = myMap.findDistance(node.getRoomName(), endName);
					list.add(node);
				}
			}
			myMap.sortByDistance(list);
			temp.isExpanded = true;
			
		}//end while
		
		return false;
	}

}
