package com.fmi.project.model;

public class Transition {
	private Room startRoom;
    private Room toRoom;
    private TransitType type;
    private double cost;
    

    public Transition() {
    }

    public Transition(Room startRoom ,Room toRoom, TransitType type, double cost) {
        this.toRoom = toRoom;
        this.type = type;
        this.cost = cost;     
        this.startRoom = startRoom;
    }

    public Room getToRoom() {
        return toRoom;
    }

    public void setToRoom(Room toRoom) {
        this.toRoom = toRoom;
    }

    public TransitType getType() {
        return type;
    }

    public void setType(TransitType type) {
        this.type = type;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

	public Room getStartRoom() {
		return startRoom;
	}

	public void setStartRoom(Room startRoom) {
		this.startRoom = startRoom;
	}
    

}
