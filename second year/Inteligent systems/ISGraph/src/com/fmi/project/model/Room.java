package com.fmi.project.model;

import java.util.ArrayList;

public class Room {
    private String roomName;
    private double x;
    private double y;
    private int floorNum;
    private RoomType type;
    
    boolean isTested = false;
    boolean isExpanded = false;
    int depth = -1;
	double distanceToGoal = 0.0;
	Room parent = null;

    public ArrayList<Transition> transitions = new ArrayList<>();
    
    public Room() {
    }

    public Room(String roomName, double x, double y, int floorNum, RoomType type) {
        this.roomName = roomName;
        this.x = x;
        this.y = y;
        this.floorNum = floorNum;
        this.type = type;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public int getFloorNum() {
        return floorNum;
    }

    public void setFloorNum(int floorNum) {
        this.floorNum = floorNum;
    }

    public RoomType getType() {
        return type;
    }

    public void setType(RoomType type) {
        this.type = type;
    }

	public ArrayList<Transition> getTransitions() {
		return transitions;
	}

	public void setTransitions(ArrayList<Transition> transitions) {
		this.transitions = transitions;
	}    
	
	public void reset() {
		isTested = false;
		isExpanded = false;
		depth = -1;
		distanceToGoal = 0.0;
		parent = null;
	}
	
}
