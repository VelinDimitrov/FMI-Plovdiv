package com.fmi.project.model;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class Graph {
	public HashMap<String, Room> myGraph = new HashMap<>();
	private List<Transition> transitions = new ArrayList<>();
	
	Comparator<Room> byName = (Room n1, Room n2) -> n1.getRoomName().compareTo(n2.getRoomName());
	Comparator<Room> byDistance = (Room n1, Room n2) -> Double.compare(n1.distanceToGoal, n2.distanceToGoal);

	public void addRoom(Room room) {
		if (room == null || myGraph.containsKey(room.getRoomName())) {
			System.err.println("Room name already exists");
			return;
		}
		myGraph.put(room.getRoomName(), room);
	}
	
	public List<Transition> getTransitionsToRoom(String toRoomName){
		return transitions.stream()
			    .filter(p -> p.getToRoom().getRoomName().equals(toRoomName)).collect(Collectors.toList());
	}
	
	public boolean containsLiftTransition(String roomName, ArrayList<Transition> transitions) {
		
		return transitions.stream().filter(t -> t.getType().equals(TransitType.LIFT) && t.getToRoom().getRoomName().equals(roomName)).findAny().isPresent();
	}

	public void addTransition(String startRoomName, String endRoomName, boolean isBiDirectional, double cost,
			TransitType type) {
		if (myGraph.containsKey(startRoomName) && myGraph.containsKey(endRoomName)) {
			Room startNode = myGraph.get(startRoomName);
			Room endNode = myGraph.get(endRoomName);
			
			Transition transition = new Transition(startNode,endNode, type, cost);
			startNode.getTransitions().add(transition);
			transitions.add(transition);
			
			if (isBiDirectional) {
				Transition secondTransition = new Transition(endNode,startNode, type, cost);
				endNode.getTransitions().add(secondTransition);		
				transitions.add(secondTransition);
			}
		} else {
			System.err.println("Wrong or missing room names");
		}
	}

	public Room getRoom(String name) {
		return myGraph.get(name);
	}

	public boolean containsNode(String name) {
		return myGraph.containsKey(name);
	}

	public ArrayList<Room> getLinkedTransitions(String name) {
		ArrayList<Room> linkedNodes = new ArrayList<>();
		Room node = myGraph.get(name);
		for (Transition l : node.getTransitions()) {
			linkedNodes.add(myGraph.get(l.getToRoom().getRoomName()));
		}
		return linkedNodes;
	}

	public double findDistance(String nameOne, String nameTwo) {

		Room nodeOne = getRoom(nameOne);
		Room nodeTwo = getRoom(nameTwo);

		double distance = Math
				.sqrt(Math.pow(nodeOne.getX() - nodeTwo.getX(), 2) + Math.pow(nodeOne.getY() - nodeTwo.getY(), 2)
						+ Math.pow(nodeOne.getFloorNum() - nodeTwo.getFloorNum(), 2));
		return distance;
	}

	public void resetAllNodes() {
		myGraph.forEach((k, v) -> v.reset());
	}

	public void setDepths(String name) {
		Room node = getRoom(name);
		for (Room n : getLinkedTransitions(name)) {
			if (n.depth == -1) {
				n.depth = node.depth + 1;
			}
		}
	}

	public void sortByDistance(ArrayList<Room> list) {

		list.sort(byDistance.thenComparing(byName));
	}

	public void printPath(Room current) {
		ArrayList<String> path = new ArrayList<>();
		double cost = 0;
		int nbTransitionWalk = 0;
		int nbTransitionLift = 0;
		int nbTransitionClimb = 0;

		while (current.depth != 0) {
			path.add(0, current.getRoomName());

			for (Transition trans :  getTransitionsToRoom(current.getRoomName())) {
				Room node = trans.getStartRoom();
				if (node.depth == current.depth - 1) {
					cost += trans.getCost();
					switch (trans.getType()) {
					case CLIMB:
						nbTransitionClimb++;
						break;
					case WALK:
						nbTransitionWalk++;
						break;
					case LIFT:
						nbTransitionLift++;
						break;
					}
					current = node;
					break;
				}
			}
		}

		path.add(0, current.getRoomName());
		System.out
				.println("The path is : " + path.stream().map(Object::toString).collect(Collectors.joining("----->")));
		System.out.println("Cost :" + cost);
		System.out.println("Types of path :");
		System.out.println("Walk : " + nbTransitionWalk);
		System.out.println("Climb : " + nbTransitionClimb);
		System.out.println("Lift : " + nbTransitionLift);
	}

	public void printRestrictedPath(Room current, TransitType restrictedType) {
		ArrayList<String> path = new ArrayList<>();
		double cost = 0;
		int nbTransitionWalk = 0;
		int nbTransitionLift = 0;
		int nbTransitionClimb = 0;

		while (current.depth != 0) {
			path.add(0, current.getRoomName());

			for (Transition trans : getTransitionsToRoom(current.getRoomName())) {
				Room node = trans.getStartRoom();
				if (node.getType().equals(RoomType.TRANSIT) && trans.getType().equals(restrictedType)) {
					continue;
				} 			
				if (node.depth == current.depth - 1) {
					cost += trans.getCost();
					switch (trans.getType()) {
					case CLIMB:
						nbTransitionClimb++;
						break;
					case WALK:
						nbTransitionWalk++;
						break;
					case LIFT:
						nbTransitionLift++;
						break;
					}
					current = node;
					break;
				}
			}
		}

		path.add(0, current.getRoomName());
		System.out
				.println("The path is : " + path.stream().map(Object::toString).collect(Collectors.joining("----->")));
		System.out.println("Cost :" + cost);
		System.out.println("Types of path :");
		System.out.println("Walk : " + nbTransitionWalk);
		System.out.println("Climb : " + nbTransitionClimb);
		System.out.println("Lift : " + nbTransitionLift);
	}
	
	public void printPathThirdExcercise(Room current) {
		ArrayList<String> path = new ArrayList<>();
		double cost = 0;
		int nbTransitionWalk = 0;
		int nbTransitionLift = 0;
		int nbTransitionClimb = 0;

		while (current.depth != 0) {
			path.add(0, current.getRoomName());

			for (Transition trans :  getTransitionsToRoom(current.getRoomName())) {
				Room node = trans.getStartRoom();				
				if (node.depth == current.depth - 1) {
					if (!containsLiftTransition(node.getRoomName(),current.getTransitions()) && trans.getType().equals(TransitType.CLIMB)) {
						cost += trans.getCost();
					}else if (containsLiftTransition(node.getRoomName(),current.getTransitions()) && trans.getType().equals(TransitType.CLIMB)) {
						continue;
					}
					cost += trans.getCost();
					switch (trans.getType()) {
					case CLIMB:
						nbTransitionClimb++;
						break;
					case WALK:
						nbTransitionWalk++;
						break;
					case LIFT:
						nbTransitionLift++;
						break;
					}
					current = node;
					break;
				}
			}
		}

		path.add(0, current.getRoomName());
		System.out
				.println("The path is : " + path.stream().map(Object::toString).collect(Collectors.joining("----->")));
		System.out.println("Cost :" + cost);
		System.out.println("Types of path :");
		System.out.println("Walk : " + nbTransitionWalk);
		System.out.println("Climb : " + nbTransitionClimb);
		System.out.println("Lift : " + nbTransitionLift);
	}
}
