package com.fmi.project.model;

public enum TransitType {
    WALK,
    CLIMB,
    LIFT;
    
    public static TransitType getType(String type) {
        switch(type) {
            case "lift": return LIFT;
            case "climb": return CLIMB;
            case "walk":  return WALK;
        }
        throw new RuntimeException("Transit type is not implemented");
    }
    
}
