package com.fmi.project.model;

import java.util.ArrayList;

public class WithoutSingleTransitWaySearch implements Searchable {
	
	Graph myMap;
	TransitType restrictedType;
	
	public WithoutSingleTransitWaySearch(Graph myMap, TransitType type) {
		super();
		this.myMap = myMap;
		this.restrictedType = type;
	}
	
	@Override
	public boolean search(String startName, String endName) {
		
		if(!myMap.containsNode(startName) || !myMap.containsNode(endName)) {
			return false;
		}
		
		Room startNode = myMap.getRoom(startName);
		ArrayList<Room> list = new ArrayList<>();
		list.add(startNode);
		startNode.depth = 0;
		
		Room temp;
		
		while(!list.isEmpty()) {
			temp = list.get(0);
			System.out.println("Current room is :" + temp.getRoomName() );
			
			if(temp.getRoomName().equals(endName)) {
				myMap.printRestrictedPath(temp,restrictedType);
				return true;
			}
			myMap.setDepths(temp.getRoomName());
			temp.isTested = true;
			list.remove(0);
			
			for(Transition trans : temp.getTransitions()) {
				Room node = trans.getToRoom();
				if(!node.isTested && !list.contains(node)) {
					if (node.getType().equals(RoomType.TRANSIT) && trans.getType().equals(restrictedType)) {
						continue;
					}
					list.add(node);
				}
			}
			
		}//end while
		
		return false;
	}

}
