package com.fmi.project.model;

public interface Searchable {
	
	public boolean search(String startName,String endName);

}
