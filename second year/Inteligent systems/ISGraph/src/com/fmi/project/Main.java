package com.fmi.project;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import com.fmi.project.model.Graph;
import com.fmi.project.model.Room;
import com.fmi.project.model.RoomType;
import com.fmi.project.model.Searchable;
import com.fmi.project.model.SecondExcerciseSearch;
import com.fmi.project.model.ThirdExcerciseSearch;
import com.fmi.project.model.TransitType;
import com.fmi.project.model.WithoutSingleTransitWaySearch;

public class Main {

	static Graph graph = new Graph();

	public static void main(String[] args) {
		InputStream resourceAsStream = Main.class.getResourceAsStream("test.txt");
		BufferedReader in = new BufferedReader(new InputStreamReader(resourceAsStream));
		try {
			String line;
			while ((line = in.readLine()) != null) {
				line = line.substring(0, line.length() - 1);
				String[] tokens = line.split(", ");
				if (line.contains("yes") || line.contains("no")) {					
					graph.addTransition(tokens[0], tokens[1], tokens[4].equals("yes") ? Boolean.TRUE : Boolean.FALSE,
							Double.valueOf(tokens[3]), TransitType.getType(tokens[2]));
				} else {
					graph.addRoom(new Room(tokens[0], Double.valueOf(tokens[1]), Double.valueOf(tokens[2]),
							Integer.valueOf(tokens[3]), (tokens[4].equals("room") ? RoomType.ROOM : RoomType.TRANSIT)));
				}
			}
		} catch (IOException e) {
			System.out.println("line is not correct");
		}
		
		System.out.println("First Search \n");
		findPath("102", "504", new WithoutSingleTransitWaySearch(graph,TransitType.LIFT));
		System.out.println("\n\n\n\n\n\n");
		System.out.println("Second Search \n");
		findPath("102", "504", new SecondExcerciseSearch(graph));
		System.out.println("\n\n\n\n\n\n");
		System.out.println("Third Search \n");
		findPath("102", "504", new ThirdExcerciseSearch(graph));
	}
	
	public static void findPath(String startName, String endName,
			Searchable searcher) {
		graph.resetAllNodes();
		if(searcher.search(startName, endName)) {
			System.out.println("HAVE A PATH");
		}else {
			System.out.println("THERE IS NO PATH");
		}
	}

}
