package com.graph.data;

import com.graph.model.Transition;
import org.supercsv.cellprocessor.ParseBool;
import org.supercsv.cellprocessor.ParseDouble;
import org.supercsv.cellprocessor.ParseEnum;
import org.supercsv.cellprocessor.ParseInt;
import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvBeanReader;
import org.supercsv.io.ICsvBeanReader;
import org.supercsv.prefs.CsvPreference;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class TransitDataReader {
    private static CellProcessor[] getProcessors() {
        return new CellProcessor[]{
                new NotNull(),
                new NotNull(),
                new ParseEnum(),
                new ParseDouble(),
                new ParseBool()
        };
    }

    public List<Transition> parseTransitFile(String transitionFileName) {
        List<Transition> transitions = new ArrayList<>();
        Transition transition;

        try (ICsvBeanReader beanReader = new CsvBeanReader(new FileReader(transitionFileName), CsvPreference.STANDARD_PREFERENCE)) {

            final String[] header = beanReader.getHeader(true);
            final CellProcessor[] processors = getProcessors();

            while ((transition = beanReader.read(Transition.class, header, processors)) != null) {
                transitions.add(transition);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return transitions;
    }
}
