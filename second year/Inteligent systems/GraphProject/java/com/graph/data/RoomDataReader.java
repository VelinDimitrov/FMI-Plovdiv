package com.graph.data;

import com.graph.model.Room;
import org.supercsv.cellprocessor.ParseDouble;
import org.supercsv.cellprocessor.ParseEnum;
import org.supercsv.cellprocessor.ParseInt;
import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvBeanReader;
import org.supercsv.io.ICsvBeanReader;
import org.supercsv.prefs.CsvPreference;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class RoomDataReader {
    private static CellProcessor[] getProcessors() {
        return new CellProcessor[]{
                new NotNull(),
                new ParseDouble(),
                new ParseDouble(),
                new ParseInt(),
                new ParseEnum()
        };
    }

    public List<Room> parseRoomFile(String roomFileName) {
        List<Room> rooms = new ArrayList<>();
        Room room;

        try (ICsvBeanReader beanReader = new CsvBeanReader(new FileReader(roomFileName), CsvPreference.STANDARD_PREFERENCE)) {

            final String[] header = beanReader.getHeader(true);
            final CellProcessor[] processors = getProcessors();

            while ((room = beanReader.read(Room.class, header, processors)) != null) {
                rooms.add(room);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return rooms;
    }
}
