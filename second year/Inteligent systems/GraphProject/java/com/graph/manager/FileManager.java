package com.graph.manager;

import com.graph.properties.PropertiesReader;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

public class FileManager {
    public static File[] allFiles() throws IOException {
        Properties prop = PropertiesReader.properties();
        File files = new File(prop.getProperty("file.path"));
        File[] getFiles = files.listFiles();
        return getFiles;
    }
}
