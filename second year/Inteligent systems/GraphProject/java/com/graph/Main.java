package com.graph;

public class Main {
	
	static Graph graph = new Graph();
	
	public static void findPath(String startName, String endName,
			Searchable searcher) {
		graph.resetAllNodes();
		if(searcher.search(startName, endName)) {
			System.out.println("HAVE A PATH");
		}else {
			System.out.println("THERE IS NO PATH");
		}
	}
    public static void main(String[] args) {

    }
}
