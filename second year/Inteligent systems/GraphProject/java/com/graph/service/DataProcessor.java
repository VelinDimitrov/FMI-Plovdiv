package com.graph.service;

import com.graph.data.RoomDataReader;
import com.graph.data.TransitDataReader;

import java.io.File;

public class DataProcessor {
    RoomDataReader roomDataReader = new RoomDataReader();
    TransitDataReader transitDataReader = new TransitDataReader();

    public void readFiles(File[] path) {
        for (int i = 0; i < path.length; i++) {
            if (path[i].isFile() && path[i].getName().contains("room")) {
                roomDataReader.parseRoomFile(path[i].toString());
            } else if (path[i].isFile() && path[i].getName().contains("transit")) {
                transitDataReader.parseTransitFile(path[i].toString());
            }
        }
    }


}
