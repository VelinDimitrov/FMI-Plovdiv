#include <iostream>
using namespace std;

int avg_int(int a, int b, int c) {
	_asm {
		movsx eax,a
		add eax ,b
		add eax,c
		mov ecx ,3
		mov edx, 0
		div ecx			
	}	
}
short avg_short(short a, short b, short c) {
	_asm {
		movsx ax, a
		add ax, b
		add ax, c
		mov cx, 3
		mov dx, 0
		div cx
	}
}

int sgn(int i) {
	_asm{
		mov eax, 1
		cmp eax,i
		jle return
		mov eax, 0
		cmp eax,i
		je return
		mov eax, -1		
	return:
	}
}
int min3(unsigned char a, short b, int c) {
	_asm {
		mov al, a
		movsx bx ,b
		movsx ecx, c
		cbw
		cmp ax,bx
		jle next_num	
		movsx ax,bx
	next_num:
		cwde
		cmp eax,ecx
		jle last
		movsx eax,ecx
	last:
	}
}
int positive(int a, int b, int c) {
	_asm {
		mov eax,0
		cmp eax,a
		jg zero
		cmp eax,b
		jg zero
		cmp eax,c
		jg zero
		mov eax,1
	zero:		
	}
}
int power(int n, unsigned int m) {
	_asm {
		movsx eax,n
		mov ecx,m
		for:
		cmp ecx,1
		jle return
		mul n
		dec ecx
		jmp for
		return:
	}
}

int main() {
	cout << "Yas!!!!!!!!!!!" << endl;
	cout << avg_int(-3,4,5) << endl;
	cout << avg_short(-3, 4, 5) << endl;
	cout << sgn(-3) << endl;
	cout << sgn(0) << endl;
	cout << sgn(3) << endl;
	cout << min3('4',2,-3) << endl;
	cout << positive(-5,2,4) << endl;
	cout << power(2,3) << endl;
	char answer;
	cin >> answer;
}