#include<iostream>
using namespace std;

int abss(int value) {
	_asm {
		mov eax, value
		cmp eax, 0
		jge skip
		neg eax
		skip :
		mov value, eax	
	}

}

int addInts(int a,int b) {
	_asm {
		mov eax, a
		add eax, b
		mov a, eax			
	}
	return a;
}

int power(int x) {
	_asm {
		mov eax, x
		imul x
		mov x,eax
	}
	return x;
} 

int perimeter_of_rectangle(int a, int b) {
	_asm {
		mov eax, a
		add eax, a
		add eax, b
		add eax, b	
		mov a, eax
	}
	return a;
}

int area_of_rectangle(int a, int b) {
	_asm{
		mov eax , a
		mul b
		mov a ,eax
	}
	return a;
}
int perimeter_of_square(int a) {
	_asm {
		mov eax, a
		mov ebx, 4
		mul ebx
		mov a, eax
	}
}

int area_of_square(int a) {
	_asm {
		mov eax, a		
		mul a
		mov a, eax
	}
}

int perimeter_of_triangle(int a, int b, int c) {
	_asm {
		mov eax, a
		add eax,b
		add eax,c
		mov a, eax
	}
}

int perimeter_of_triangle2(int a) {
	_asm {
		mov eax, a
		mov ecx, 3
		mul ecx
	}
}

int perimeter_of_triangle3(int a,int b) {
	int result = 0;
	int c = 0;
	_asm {
		mov eax, a
		mul a
		mov ecx, eax
		mov eax,b
		mul b
		add eax, ecx
		mov c , eax
		mov eax, a
		add eax, b
		mov result, eax
	}
	result += sqrt(c);
	return result;
}

int perimeter_of_triangle4(int a, int b,int c) {
	// P = sqrt(s*(s-a)*(s-b)*(s-c) ); s = (a + b + c)/2;
	int result = 0;
	_asm {
		mov eax, a
		add eax,b
		add eax,c
		mov ecx, 2
		div ecx
		mov ecx ,eax
		sub ecx ,a
		mul ecx
		add ecx ,a
		sub ecx ,b
		mul ecx
		add ecx, b
		sub ecx, c
		mul ecx
		mov result ,eax
	}
	result = sqrt(result);
	return result;
}
int area_of_cube(int a) {
	_asm {
		mov eax, a
		mul a
		mov ecx,6
		mul ecx
	}
}
int main() 
{	
	cout << "Hello, world \n";

	cout << abss(-5) << endl;

	cout << addInts(3, 4) << endl;

	cout << power(-8) << endl;

	cout << perimeter_of_rectangle(2,3) << endl;

	cout << area_of_rectangle(4, 5) << endl;

	cout << perimeter_of_square(4) << endl;


	cout << area_of_square(5) << endl;

	cout << perimeter_of_triangle(5,6,7) << endl;

	cout << perimeter_of_triangle2(5) << endl;

	cout << perimeter_of_triangle3(5, 6) << endl;

	cout << area_of_cube(2) << endl;

	cout << perimeter_of_triangle4(1, 2, 3) << endl;

	char answer = 0;
	cin >> answer;

}