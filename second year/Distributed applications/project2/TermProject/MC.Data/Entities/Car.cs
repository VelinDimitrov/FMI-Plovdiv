﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MC.Data.Entities
{
    public class Car : BaseEntity
    {
        [Required]
        [MaxLength(200)]
        [MinLength(1)]
        public string Model { get; set; }
        [Required]
        public string ReleaseYear { get; set; }
        public int HorsePower { get; set; }

        public int MakeId { get; set; }
        public virtual Make Make { get; set; }

        public int TypeId { get; set; }
        public virtual Type Type { get; set; }

        public Car() { }
    }
}
