﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MC.Data.Entities;

namespace MC.Data.Contexts
{
    public class TermProjectDbContext : DbContext
    {
        public DbSet<Car> Cars { get; set; }
        public DbSet<Entities.Type> Types { get; set; }
        public DbSet<Make> Makes { get; set; }
    }
}
