﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using MC.ApplicationServices.DTOs;
using MC.ApplicationServices.Implementations;

namespace MC.WcfServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Type" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Type.svc or Type.svc.cs at the Solution Explorer and start debugging.
    public class Type : IType
    {
        #region Fields
        private TypeManagementService service = new TypeManagementService();
        #endregion

        public string DeleteType(int id)
        {
            if (!service.Delete(id))
                return "Type is not deleted";

            return "Type is deleted";
        }

        public TypeDto GetTypeByID(int id)
        {
            return service.GetById(id);
        }

        public List<TypeDto> GetTypes()
        {
            return service.Get();
        }

        public string PostType(TypeDto typeDto)
        {
            if (!service.Save(typeDto))
                return "Type is not inserted";

            return "Type is inserted";
        }

        public string PutType(TypeDto typeDto)
        {
            if (!service.Update(typeDto))
                return "Type is not updated";

            return "Type is updated";
        }
    }
}
