﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using MC.ApplicationServices.DTOs;
using MC.ApplicationServices.Implementations;

namespace MC.WcfServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Car" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Car.svc or Car.svc.cs at the Solution Explorer and start debugging.
    public class Car : ICar
    {
        #region Fields
        private CarManagementService service = new CarManagementService();
        #endregion

        public string DeleteCar(int id)
        {
            if (!service.Delete(id))
                return "Car is not deleted";

            return "Car is deleted";
        }

        public CarDto GetCarByID(int id)
        {
            return service.GetById(id);
        }

        public List<CarDto> GetCars()
        {
            return service.Get();
        }

        public string PostCar(CarDto carDto)
        {
            if (!service.Save(carDto))
                return "Car is not inserted";

            return "Car is inserted";
        }

        public string PutCar(CarDto carDto)
        {
            if (!service.Update(carDto))
                return "Car is not updated";

            return "Car is updated";
        }
    }
}
