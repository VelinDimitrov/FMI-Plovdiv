﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using MC.ApplicationServices.DTOs;
using MC.ApplicationServices.Implementations;

namespace MC.WcfServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Make" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Make.svc or Make.svc.cs at the Solution Explorer and start debugging.
    public class Make : IMake
    {
        #region Fields
        private MakeManagementService service = new MakeManagementService();
        #endregion

        public string DeleteMake(int id)
        {
            if (!service.Delete(id))
                return "Movie is not deleted";

            return "Movie is deleted";
        }

        public MakeDto GetMakeByID(int id)
        {
            return service.GetById(id);
        }

        public List<MakeDto> GetMakes()
        {
            return service.Get();
        }

        public string PostMake(MakeDto makeDto)
        {
            if (!service.Save(makeDto))
                return "Make is not inserted";

            return "Make is inserted";
        }

        public string PutMake(MakeDto makeDto)
        {
            if (!service.Update(makeDto))
                return "Make is not updated";

            return "Make is updated";
        }
    }
}
