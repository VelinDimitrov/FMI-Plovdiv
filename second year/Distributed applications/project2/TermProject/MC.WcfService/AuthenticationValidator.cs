﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Selectors;
using System.Linq;
using System.ServiceModel;
using System.Web;

namespace MC.WcfServices
{
    public class AuthenticationValidator : UserNamePasswordValidator
    {
        public override void Validate(string userName, string password)
        {
            if (null == userName || null == password)
            {
                throw new ArgumentNullException();
            }

            if (!(userName == "admin" && password == "admin") && !(userName == "user" && password == "user"))
            {
                throw new FaultException("Unknown Username or Incorrect Password");
            }
        }
    }
}