﻿using MC.ApplicationServices.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace MC.WcfServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ICar" in both code and config file together.
    [ServiceContract]
    public interface ICar
    {
        [OperationContract]
        List<CarDto> GetCars();

        [OperationContract]
        string PostCar(CarDto carDto);

        [OperationContract]
        string PutCar(CarDto carDto);

        [OperationContract]
        string DeleteCar(int id);

        [OperationContract]
        CarDto GetCarByID(int id);
    }
}
