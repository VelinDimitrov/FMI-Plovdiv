﻿using MC.Data.Contexts;
using MC.Data.Entities;
using System;

namespace MC.Repositories.Implementations
{
    public class UnitOfWork : IDisposable
    {
        private TermProjectDbContext context = new TermProjectDbContext();
        private GenericRepository<Car> carRepository;
        private GenericRepository<Data.Entities.Type> typeRepository;
        private GenericRepository<Make> makeReposiotry;

        public GenericRepository<Car> CarRepository
        {
            get
            {

                if (this.carRepository == null)
                {
                    this.carRepository = new GenericRepository<Car>(context);
                }
                return carRepository;
            }
        }

        public GenericRepository<Data.Entities.Type> TypeReposiotry
        {
            get
            {
                if (this.typeRepository == null)
                {
                    this.typeRepository = new GenericRepository<Data.Entities.Type>(context);
                }
                return typeRepository;
            }
        }

        public GenericRepository<Make> MakeReposiotry
        {
            get
            {
                if (this.makeReposiotry == null)
                {
                    this.makeReposiotry = new GenericRepository<Make>(context);
                }
                return makeReposiotry;
            }
        }

        public void Save()
        {
            context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
