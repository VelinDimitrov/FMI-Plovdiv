﻿using MC.View.TypeServiceReference;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MC.View.ViewModels
{
    public class TypeVM
    {
        #region Properties
        public int Id { get; set; }      

        [Required]
        [MaxLength(100)]
        [Display(Name = "Type Name")]
        public string Name { get; set; }
        [Required]
        [MaxLength(400)]
        [Display(Name = "Description")]
        public string Description { get; set; }
        #endregion

        #region Contructors
        public TypeVM() { }

        public TypeVM(TypeDto typeDto)
        {
            Id = typeDto.Id;
            Name = typeDto.Name;
            Description = typeDto.Description;
        }
        #endregion
    }
}