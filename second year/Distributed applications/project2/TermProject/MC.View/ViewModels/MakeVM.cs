﻿using MC.View.MakeServiceReference;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MC.View.ViewModels
{
    public class MakeVM
    {
        #region Properties
        public int Id { get; set; }

        [Required]
        [MaxLength(100)]
        [Display(Name = "Make Name")]
        public string Name { get; set; }
        public string Description { get; set; }
        public string Country { get; set; }
        #endregion

        #region Contructors
        public MakeVM() { }

        public MakeVM(MakeDto makeDto)
        {
            Id = makeDto.Id;
            Name = makeDto.Name;
            Description = makeDto.Description;
            Country = makeDto.Country;
        }
        #endregion
    }
}