﻿using MC.View.CarServiceReference;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MC.View.ViewModels
{
    public class CarVM
    {
        #region Properties
        public int Id { get; set; }
        [Required]
        [MaxLength(200)]
        public string Model { get; set; }

        [Required]
        [Display(Name = "Release year")]
        public string ReleaseYear { get; set; }

        [Display(Name = "Horse power")]
        public int HorsePower { get; set; }

        [Display(Name = "Make")]
        public int MakeId { get; set; }
        public MakeVM MakeVM { get; set; }

        [Display(Name = "Type")]
        public int TypeId { get; set; }
        public TypeVM TypeVM { get; set; }
        #endregion

        #region Contructors
        public CarVM() { }

        public CarVM(CarDto carDto)
        {
            Id = carDto.Id;
            ReleaseYear = carDto.ReleaseYear;
            HorsePower = carDto.HorsePower;
            Model = carDto.Model;
            MakeId = carDto.Make.Id;
            MakeVM = new MakeVM
            {
                Id = carDto.Make.Id,
                Name = carDto.Make.Name,
                Description =carDto.Make.Description,
                Country = carDto.Make.Country
            };
            TypeId = carDto.Type.Id;
            TypeVM = new TypeVM
            {
                Id = carDto.Type.Id,
                Name = carDto.Type.Name,                
                Description = carDto.Type.Description
                
            };
        }
        #endregion

    }
}