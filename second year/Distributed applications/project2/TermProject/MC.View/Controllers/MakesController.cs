﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MC.Data.Contexts;
using MC.Data.Entities;
using MC.View.ViewModels;

namespace MC.View.Controllers
{
    public class MakesController : Controller
    {
        private TermProjectDbContext db = new TermProjectDbContext();
        // GET: Types
        public ActionResult Index()
        {
            List<MakeVM> makesVM = new List<MakeVM>();
            using (MakeServiceReference.MakeClient service = new MakeServiceReference.MakeClient())
            {
                foreach (var item in service.GetMakes())
                {
                    makesVM.Add(new MakeVM(item));
                }
            }
            return View(makesVM);
        }

        // GET: Types/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            using (MakeServiceReference.MakeClient service = new MakeServiceReference.MakeClient())
            {
                return View(new MakeVM(service.GetMakeByID(id.Value)));
            }
        }

        // GET: Types/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Types/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Description,Country")] MakeVM makeVM)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    using (MakeServiceReference.MakeClient service = new MakeServiceReference.MakeClient())
                    {
                        MakeServiceReference.MakeDto makeDto = new MakeServiceReference.MakeDto
                        {
                            Description = makeVM.Description,
                            Name = makeVM.Name,
                            Id = makeVM.Id,
                            Country = makeVM.Country
                        };
                        service.PostMake(makeDto);
                    }

                    return RedirectToAction("Index");
                }

                return View();
            }
            catch
            {
                return View();
            }
        }

        // GET: Types/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            using (MakeServiceReference.MakeClient service = new MakeServiceReference.MakeClient())
            {
                return View(new MakeVM(service.GetMakeByID(id.Value)));
            }
        }

        // POST: Types/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Description,Country")] MakeVM makeVM)
        {
            if (ModelState.IsValid)
            {
                using (MakeServiceReference.MakeClient service = new MakeServiceReference.MakeClient())
                {
                    MakeServiceReference.MakeDto makeDto = new MakeServiceReference.MakeDto
                    {
                        Description = makeVM.Description,
                        Name = makeVM.Name,
                        Id = makeVM.Id,
                        Country = makeVM.Country
                    };
                    service.PutMake(makeDto);
                }

                return RedirectToAction("Index");
            }
            return View(makeVM);
        }

        // GET: Types/Delete/5
        public ActionResult Delete(int? id)
        {
            using (MakeServiceReference.MakeClient service = new MakeServiceReference.MakeClient())
            {
                service.DeleteMake(id.Value);
            }
            return RedirectToAction("Index");
        }

        // POST: Types/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            using (MakeServiceReference.MakeClient service = new MakeServiceReference.MakeClient())
            {
                service.DeleteMake(id);
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
