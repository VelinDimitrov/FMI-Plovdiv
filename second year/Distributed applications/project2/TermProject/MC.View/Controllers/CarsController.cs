﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MC.Data.Contexts;
using MC.Data.Entities;
using MC.View.MakeServiceReference;
using MC.View.ViewModels;

namespace MC.View.Controllers
{
    public class CarsController : Controller
    {
        private TermProjectDbContext db = new TermProjectDbContext();

        // GET: Types
        public ActionResult Index()
        {
            List<CarVM> carsVM = new List<CarVM>();
            using (CarServiceReference.CarClient service = new CarServiceReference.CarClient())
            {
                foreach (var item in service.GetCars())
                {
                    carsVM.Add(new CarVM(item));
                }
            }
            return View(carsVM);
        }

        // GET: Types/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            using (CarServiceReference.CarClient service = new CarServiceReference.CarClient())
            {
                return View(new CarVM(service.GetCarByID(id.Value)));
            }
        }

        // GET: Types/Create
        public ActionResult Create()
        {
            using (MakeServiceReference.MakeClient service = new MakeServiceReference.MakeClient())
            {
                ViewBag.Makes = new SelectList(service.GetMakes(), "Id", "Name");
            }

            using (TypeServiceReference.TypeClient service = new TypeServiceReference.TypeClient())
            {
                ViewBag.Types = new SelectList(service.GetTypes(), "Id", "Name");
            }
            return View();
        }

        // POST: Types/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind] CarVM carVM)
        {
            try
            {
                if (ModelState.IsValid)
                {                    
                    using (CarServiceReference.CarClient service = new CarServiceReference.CarClient())
                    {                       
                        CarServiceReference.CarDto carDto = new CarServiceReference.CarDto
                        {
                            Id = carVM.Id,
                            HorsePower = carVM.HorsePower,
                            Model = carVM.Model,
                            ReleaseYear = carVM.ReleaseYear,
                            Make = new CarServiceReference.MakeDto
                            {
                                Id = carVM.MakeId
                            },
                            Type = new CarServiceReference.TypeDto
                            {
                                Id = carVM.TypeId
                            }
                        };
                        service.PostCar(carDto);
                    }

                    return RedirectToAction("Index");
                }

                return RedirectToAction("Create");
            }
            catch
            {
                return View();
            }
        }

        // GET: Types/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            using (MakeServiceReference.MakeClient makeService = new MakeServiceReference.MakeClient()) 
            using (TypeServiceReference.TypeClient typeService = new TypeServiceReference.TypeClient())           
            using (CarServiceReference.CarClient service = new CarServiceReference.CarClient())
            {
                ViewBag.Makes = new SelectList(makeService.GetMakes(), "Id", "Name");
                ViewBag.Types = new SelectList(typeService.GetTypes(), "Id", "Name");
                return View(new CarVM(service.GetCarByID(id.Value)));
            }
        }

        // POST: Types/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind] CarVM carVM)
        {
            if (ModelState.IsValid)
            {            
                using (CarServiceReference.CarClient service = new CarServiceReference.CarClient())
                {
                    CarServiceReference.CarDto carDto = new CarServiceReference.CarDto
                    {

                        Id = carVM.Id,
                        HorsePower = carVM.HorsePower,
                        Model = carVM.Model,
                        ReleaseYear = carVM.ReleaseYear,
                        Make = new CarServiceReference.MakeDto
                        {
                            Id = carVM.MakeId
                        },
                        Type = new CarServiceReference.TypeDto
                        {
                            Id = carVM.TypeId
                        }
                    };
                    service.PutCar(carDto);
                }

                return RedirectToAction("Index");
            }
            return View(carVM);
        }

        // GET: Types/Delete/5
        public ActionResult Delete(int? id)
        {
            using (CarServiceReference.CarClient service = new CarServiceReference.CarClient())
            {
                service.DeleteCar(id.Value);
            }
            return RedirectToAction("Index");
        }

        // POST: Types/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            using (CarServiceReference.CarClient service = new CarServiceReference.CarClient())
            {
                service.DeleteCar(id);
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
