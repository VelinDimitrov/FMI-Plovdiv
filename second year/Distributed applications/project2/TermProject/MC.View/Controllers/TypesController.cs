﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MC.Data.Contexts;
using MC.Data.Entities;
using MC.View.ViewModels;

namespace MC.View.Controllers
{
    public class TypesController : Controller
    {
        private TermProjectDbContext db = new TermProjectDbContext();

        // GET: Types
        public ActionResult Index()
        {
            List<TypeVM> typesVM = new List<TypeVM>();
            using (TypeServiceReference.TypeClient service = new TypeServiceReference.TypeClient())
            {
                foreach (var item in service.GetTypes())
                {
                    typesVM.Add(new TypeVM(item));
                }
            }
            return View(typesVM);
        }

        // GET: Types/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
                        
            using (TypeServiceReference.TypeClient service = new TypeServiceReference.TypeClient())
            {
                return View(new TypeVM(service.GetTypeByID(id.Value)));
            }           
        }

        // GET: Types/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Types/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Description")] TypeVM typeVM)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    using (TypeServiceReference.TypeClient service = new TypeServiceReference.TypeClient())
                    {
                        TypeServiceReference.TypeDto typeDto = new TypeServiceReference.TypeDto
                        {
                            Description = typeVM.Description,
                            Name = typeVM.Name,
                            Id = typeVM.Id                            
                        };
                        service.PostType(typeDto);
                    }

                    return RedirectToAction("Index");
                }

                return View();
            }
            catch
            {
                return View();
            }
        }

        // GET: Types/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            using (TypeServiceReference.TypeClient service = new TypeServiceReference.TypeClient())
            {
                return View(new TypeVM(service.GetTypeByID(id.Value)));
            }
        }

        // POST: Types/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Description")] TypeVM typeVM)
        {
            if (ModelState.IsValid)
            {
                using (TypeServiceReference.TypeClient service = new TypeServiceReference.TypeClient())
                {
                    TypeServiceReference.TypeDto typeDto = new TypeServiceReference.TypeDto
                    {
                        Description = typeVM.Description,
                        Name = typeVM.Name,
                        Id = typeVM.Id
                    };
                    service.PutType(typeDto);
                }

                return RedirectToAction("Index");
            }
            return View(typeVM);
        }

        // GET: Types/Delete/5
        public ActionResult Delete(int? id)
        {
            using (TypeServiceReference.TypeClient service = new TypeServiceReference.TypeClient())
            {
                service.DeleteType(id.Value);
            }
            return RedirectToAction("Index");
        }

        // POST: Types/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            using (TypeServiceReference.TypeClient service = new TypeServiceReference.TypeClient())
            {               
                service.DeleteType(id);
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
