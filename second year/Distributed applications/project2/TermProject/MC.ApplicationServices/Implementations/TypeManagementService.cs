﻿using MC.ApplicationServices.DTOs;
using MC.Data.Entities;
using MC.Repositories.Implementations;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MC.ApplicationServices.Implementations
{
    public class TypeManagementService
    {
        public List<TypeDto> Get()
        {
            List<TypeDto> typeDtos = new List<TypeDto>();

            using (UnitOfWork unitOfWork = new UnitOfWork())
            {
                foreach (var item in unitOfWork.TypeReposiotry.Get())
                {
                    typeDtos.Add(new TypeDto
                    {
                        Name = item.Name,
                        Description =item.Description,
                        Id = item.Id
                    });
                }
            }

            return typeDtos;
        }

        public bool Save(TypeDto typeDto)
        {
            Type type = new Type
            {
                Name = typeDto.Name,
                Description = typeDto.Description
            };

            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    unitOfWork.TypeReposiotry.Insert(type);
                    unitOfWork.Save();
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Update(TypeDto typeDto)
        {
            Type type = new Type
            {
                Id = typeDto.Id,
                Name = typeDto.Name,
                Description = typeDto.Description
            };

            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    unitOfWork.TypeReposiotry.Update(type);
                    unitOfWork.Save();
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Type type = unitOfWork.TypeReposiotry.GetByID(id);
                    unitOfWork.TypeReposiotry.Delete(type);
                    unitOfWork.Save();
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        public TypeDto GetById(int id)
        {
            TypeDto typeDto = new TypeDto();

            using (UnitOfWork unitOfWork = new UnitOfWork())
            {
                Type type = unitOfWork.TypeReposiotry.GetByID(id);
                typeDto = new TypeDto
                {
                  Id = type.Id,
                  Name = type.Name,
                  Description = type.Description
                };
            }

            return typeDto;
        }
    }
}