﻿using MC.ApplicationServices.DTOs;
using MC.Data.Entities;
using MC.Repositories.Implementations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MC.ApplicationServices.Implementations
{
    public class MakeManagementService
    {
        public List<MakeDto> Get()
        {
            List<MakeDto> makeDtos = new List<MakeDto>();

            using (UnitOfWork unitOfWork = new UnitOfWork())
            {
                foreach (var item in unitOfWork.MakeReposiotry.Get())
                {
                    makeDtos.Add(new MakeDto
                    {
                        Name = item.Name,
                        Description = item.Description,
                        Id = item.Id,
                        Country = item.Country
                    });
                }
            }

            return makeDtos;
        }

        public bool Save(MakeDto makeDto)
        {
            Make make = new Make
            {
                Id = makeDto.Id,
                Name = makeDto.Name,
                Description = makeDto.Description,
                Country = makeDto.Country
            };

            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    unitOfWork.MakeReposiotry.Insert(make);
                    unitOfWork.Save();
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Make make = unitOfWork.MakeReposiotry.GetByID(id);
                    unitOfWork.MakeReposiotry.Delete(make);
                    unitOfWork.Save();
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        public MakeDto GetById(int id)
        {
            MakeDto makeDto = new MakeDto();

            using (UnitOfWork unitOfWork = new UnitOfWork())
            {
                Make make = unitOfWork.MakeReposiotry.GetByID(id);
                makeDto = new MakeDto
                {
                    Name = make.Name,
                    Description = make.Description,
                    Id = make.Id,
                    Country = makeDto.Country
                };
            }

            return makeDto;
        }

        public bool Update(MakeDto makeDto)
        {
            Make make = new Make
            {
                Id = makeDto.Id,
                Name = makeDto.Name,
                Description = makeDto.Description,
                Country = makeDto.Country
            };

            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    unitOfWork.MakeReposiotry.Update(make);
                    unitOfWork.Save();
                }

                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}