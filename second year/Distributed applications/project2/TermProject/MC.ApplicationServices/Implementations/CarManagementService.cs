﻿using MC.ApplicationServices.DTOs;
using MC.Data.Entities;
using MC.Repositories.Implementations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MC.ApplicationServices.Implementations
{
    public class CarManagementService
    {
        public List<CarDto> Get()
        {
            List<CarDto> carDtos = new List<CarDto>();

            using (UnitOfWork unitOfWork = new UnitOfWork())
            {
                foreach (var item in unitOfWork.CarRepository.Get())
                {
                    carDtos.Add(new CarDto
                    {
                        Id = item.Id,
                        HorsePower = item.HorsePower,
                        Model = item.Model,
                        ReleaseYear = item.ReleaseYear,
                        Make = new MakeDto
                        {
                            Id =item.Make.Id,
                            Country = item.Make.Country,
                            Description = item .Make.Description,
                            Name = item.Make.Name                            
                        },
                        Type = new TypeDto
                        {
                            Id = item.Type.Id,
                            Name = item.Type.Name,
                            Description = item .Type.Description
                        }
                    });
                }
            }

            return carDtos;
        }

        public bool Save(CarDto carDto)
        {  
            using (UnitOfWork unitOfWork = new UnitOfWork())
            {
                Car car = new Car
                {
                    Id = carDto.Id,
                    HorsePower = carDto.HorsePower,
                    Model = carDto.Model,
                    ReleaseYear = carDto.ReleaseYear,
                    Make = unitOfWork.MakeReposiotry.GetByID(carDto.Make.Id),
                    Type = unitOfWork.TypeReposiotry.GetByID(carDto.Type.Id)                  
                };
                unitOfWork.CarRepository.Insert(car);
                unitOfWork.Save();
            }

            return true;

        }

        public CarDto GetById(int id)
        {
            CarDto carDto = new CarDto();

            using (UnitOfWork unitOfWork = new UnitOfWork())
            {
                Car car = unitOfWork.CarRepository.GetByID(id);
                carDto = new CarDto
                {
                    Id = car.Id,
                    HorsePower = car.HorsePower,
                    Model = car.Model,
                    ReleaseYear = car.ReleaseYear,
                    Make = new MakeDto
                    {
                        Id = car.Make.Id,
                        Country = car.Make.Country,
                        Description = car.Make.Description,
                        Name = car.Make.Name
                    },
                    Type = new TypeDto
                    {
                        Id = car.Type.Id,
                        Name = car.Type.Name,
                        Description = car.Type.Description
                    }
                };
            }

            return carDto;
        }

        public bool Delete(int id)
        {

            using (UnitOfWork unitOfWork = new UnitOfWork())
            {
                Car car = unitOfWork.CarRepository.GetByID(id);
                unitOfWork.CarRepository.Delete(car);
                unitOfWork.Save();
            }

            return true;

        }

        public bool Update(CarDto carDto)
        {

            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Car car = new Car
                    {
                        Id = carDto.Id,
                        HorsePower = carDto.HorsePower,
                        Model = carDto.Model,
                        ReleaseYear = carDto.ReleaseYear,
                        Make = unitOfWork.MakeReposiotry.GetByID(carDto.Make.Id),
                        MakeId = carDto.Make.Id,
                        Type = unitOfWork.TypeReposiotry.GetByID(carDto.Type.Id),
                        TypeId = carDto.Type.Id
                    };
                    unitOfWork.CarRepository.Update(car);
                    unitOfWork.Save();
                }

                return true;
            }
            catch(Exception e)
            {
                Console.WriteLine(e.ToString());
                return false;
            }
        }


    }
}