﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MC.ApplicationServices.DTOs
{
    public class CarDto
    {
        public int Id { get; set; }
        public string Model { get; set; }     
        public string ReleaseYear { get; set; }
        public int HorsePower { get; set; }
        public MakeDto Make { get; set; }      
        public TypeDto Type { get; set; }
    }
}