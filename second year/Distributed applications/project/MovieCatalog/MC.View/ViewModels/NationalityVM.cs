﻿using MC.View.NationalityReference;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MC.View.ViewModels
{
    public class NationalityVM
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public NationalityVM() { }
        public NationalityVM(NationalityDto nationalityDto)
        {
            this.Id = nationalityDto.Id;
            this.Name = nationalityDto.Name;
        }
    }
}