﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MC.View.FacultyReference;

namespace MC.View.ViewModels
{
    public class FacultyVM
    {     
        public int Id { get; set; }
        public string Name { get; set; }
        public string City { get; set; }
        public string Address { get; set; }

        public FacultyVM() { }
        public FacultyVM(FacultyDto facultyDto)
        {
            this.Id = facultyDto.Id;
            this.Name = facultyDto.Name;
            this.City = facultyDto.City;
            this.Address = facultyDto.Address;
        }
    }
}