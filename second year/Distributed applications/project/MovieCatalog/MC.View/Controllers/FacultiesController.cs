﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MC.Data.Contexts;
using MC.Data.Entities;
using MC.View.Filter;
using MC.View.ViewModels;

namespace MC.View
{
    [AuthenticationFilter]
    public class FacultiesController : Controller
    {
        private TermProjectDbContext db = new TermProjectDbContext();

        // GET: Faculties
        public ActionResult Index()
        {
            List<FacultyVM> typesVM = new List<FacultyVM>();
            using (FacultyReference.FacultyClient service = new FacultyReference.FacultyClient())
            {
                foreach (var item in service.GetFaculties())
                {
                    typesVM.Add(new FacultyVM(item));
                }
            }
            return View(typesVM);
        }

        // GET: Faculties/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            using (FacultyReference.FacultyClient service = new FacultyReference.FacultyClient())
            {
                return View(new FacultyVM(service.GetFacultyByID(id.Value)));
            }
        }

        // GET: Faculties/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Faculties/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind] FacultyVM faculty)
        {
            if (ModelState.IsValid)
            {
                using (FacultyReference.FacultyClient service = new FacultyReference.FacultyClient())
                {
                    FacultyReference.FacultyDto facultyDto = new FacultyReference.FacultyDto
                    {
                        Name = faculty.Name,
                        Id = faculty.Id,
                        Address = faculty.Address,
                        City = faculty.City
                    };
                    service.PostFaculty(facultyDto);
                }

                return RedirectToAction("Index");
            }

            return View(faculty);
        }

        // GET: Faculties/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            using (FacultyReference.FacultyClient service = new FacultyReference.FacultyClient())
            {
                return View(new FacultyVM(service.GetFacultyByID(id.Value)));
            }           
        }

        // POST: Faculties/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind] FacultyVM faculty)
        {
            if (ModelState.IsValid)
            {
                using (FacultyReference.FacultyClient service = new FacultyReference.FacultyClient())
                {
                    FacultyReference.FacultyDto facultyDto = new FacultyReference.FacultyDto
                    {
                        Name = faculty.Name,
                        Id = faculty.Id,
                        Address = faculty.Address,
                        City = faculty.City
                    };
                    service.PutFaculty(facultyDto);
                }

                return RedirectToAction("Index");
            }
            return View(faculty);
        }

        // GET: Faculties/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            using (FacultyReference.FacultyClient service = new FacultyReference.FacultyClient())
            {
                service.DeleteFaculty(id.Value);
            }
            return RedirectToAction("Index");
        }

        // POST: Faculties/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            using (FacultyReference.FacultyClient service = new FacultyReference.FacultyClient())
            {
                service.DeleteFaculty(id);
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
