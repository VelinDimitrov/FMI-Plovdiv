﻿using MC.View.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MC.View.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Login()
        {
            return View(new UserVM());
        }

        [HttpPost]
        public ActionResult Login(UserVM user)
        {
            if (!ModelState.IsValid)
                return View(user);

            if (user.username == "admin" && user.password == "admin")
            {
                Session["loggedUser"] = user.username;
                return RedirectToAction("Index");
            }
            else
            {
                ModelState.AddModelError("AuthenticationFailed", "Authentication failed!");
                return View(user);
            }           
        }
    }
}