﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MC.Data.Contexts;
using MC.Data.Entities;
using MC.View.Filter;
using MC.View.ViewModels;

namespace MC.View
{
    [AuthenticationFilter]
    public class StudentsController : Controller
    {
        private TermProjectDbContext db = new TermProjectDbContext();

        // GET: Students
        public ActionResult Index()
        {
            List<StudentVM> studentVMs = new List<StudentVM>();
            using (StudentReference.IStudentClient service = new StudentReference.IStudentClient())
            {
                foreach (var item in service.GetStudents())
                {
                    studentVMs.Add(new StudentVM(item));
                }
            }
            return View(studentVMs);
        }

        // GET: Students/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            using (StudentReference.IStudentClient service = new StudentReference.IStudentClient())
            {
                return View(new StudentVM(service.GetStudentByID(id.Value)));
            }
        }

        // GET: Students/Create
        public ActionResult Create()
        {
            using (NationalityReference.NationalityClient nationalityService = new NationalityReference.NationalityClient())
            using (FacultyReference.FacultyClient facultyService = new FacultyReference.FacultyClient())
            {
                ViewBag.Nationalities = new SelectList(nationalityService.GetNationalities(), "Id", "Name");
                ViewBag.Faculties = new SelectList(facultyService.GetFaculties(), "Id", "Name");
            }
                       
            return View();
        }

        // POST: Students/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind] StudentVM student)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    using (StudentReference.IStudentClient service = new StudentReference.IStudentClient())
                    {
                        StudentReference.StudentDto studentDto = new StudentReference.StudentDto
                        {
                            Id = student.Id,
                            FirstName = student.FirstName,
                            LastName = student.LastName,
                            Comment = student.Comment,
                            DateOfBirth = student.DateOfBirth,
                            faculty = new StudentReference.FacultyDto
                            {
                                Id = student.FacultyId
                            },
                            nationality = new StudentReference.NationalityDto
                            {
                                Id = student.NationalityId
                            }

                        };
                        service.PostStudent(studentDto);
                    }

                    return RedirectToAction("Index");
                }

                return RedirectToAction("Create");
            }
            catch(Exception e)
            {
                Console.WriteLine(e.ToString());
                return View();
            }
        }

        // GET: Students/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            using (StudentReference.IStudentClient service = new StudentReference.IStudentClient())
            using (NationalityReference.NationalityClient nationalityService = new NationalityReference.NationalityClient())
            using (FacultyReference.FacultyClient facultyService = new FacultyReference.FacultyClient())
            {
                ViewBag.Nationalities = new SelectList(nationalityService.GetNationalities(), "Id", "Name");
                ViewBag.Faculties = new SelectList(facultyService.GetFaculties(), "Id", "Name");
                return View(new StudentVM(service.GetStudentByID(id.Value)));
            }
        }

        // POST: Students/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind] StudentVM student)
        {

            try
            {
                if (ModelState.IsValid)
                {
                    using (StudentReference.IStudentClient service = new StudentReference.IStudentClient())
                    {
                        StudentReference.StudentDto studentDto = new StudentReference.StudentDto
                        {
                            Id = student.Id,
                            FirstName = student.FirstName,
                            LastName = student.LastName,
                            Comment = student.Comment,
                            DateOfBirth = student.DateOfBirth,
                            faculty = new StudentReference.FacultyDto
                            {
                                Id = student.FacultyId
                            },
                            nationality = new StudentReference.NationalityDto
                            {
                                Id = student.NationalityId
                            }

                        };
                        service.PutStudent(studentDto);
                    }

                    return RedirectToAction("Index");
                }

                return RedirectToAction("Create");
            }
            catch
            {
                return View();
            }
        }

        // GET: Students/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            using (StudentReference.IStudentClient service = new StudentReference.IStudentClient())
            {
                service.DeleteStudents(id.Value);
            }
            return RedirectToAction("Index");
        }

        // POST: Students/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            using (StudentReference.IStudentClient service = new StudentReference.IStudentClient())
            {
                service.DeleteStudents(id);
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
