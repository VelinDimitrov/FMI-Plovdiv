﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using MC.ApplicationServices.DTOs;
using MC.ApplicationServices.Implementations;

namespace MC.WcfServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Faculty" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Faculty.svc or Faculty.svc.cs at the Solution Explorer and start debugging.
    public class Faculty : IFaculty
    {
        #region Fields
        private FacultyManagementService service = new FacultyManagementService();
        #endregion

        public string DeleteFaculty(int id)
        {
            if (!service.Delete(id))
                return "Faculty is not deleted";

            return "Faculty is deleted";
        }

        public FacultyDto GetFacultyByID(int id)
        {
            return service.GetById(id);
        }

        public List<FacultyDto> GetFaculties()
        {
            return service.Get();
        }

        public string PostFaculty(FacultyDto facultyDto)
        {
            if (!service.Save(facultyDto))
                return "Faculty is not inserted";

            return "Faculty is inserted";
        }

        public string PutFaculty(FacultyDto facultyDto)
        {
            if (!service.Update(facultyDto))
                return "Faculty is not updated";

            return "Faculty is updated";
        }
    }
}
