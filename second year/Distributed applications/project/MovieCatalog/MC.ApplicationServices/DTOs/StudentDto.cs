﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MC.ApplicationServices.DTOs
{
    public class StudentDto
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string Comment { get; set; }
        public FacultyDto faculty { get; set; }
        public NationalityDto nationality { get; set; }
    }
}