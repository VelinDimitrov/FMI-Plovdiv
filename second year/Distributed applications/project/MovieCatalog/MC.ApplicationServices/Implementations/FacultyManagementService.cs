﻿using MC.ApplicationServices.DTOs;
using MC.Data.Entities;
using MC.Repositories.Implementations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MC.ApplicationServices.Implementations
{
    public class FacultyManagementService
    {
        public List<FacultyDto> Get()
        {
            List<FacultyDto> facultyDto = new List<FacultyDto>();

            using (UnitOfWork unitOfWork = new UnitOfWork())
            {
                foreach (var item in unitOfWork.FacultyReposiotry.Get())
                {
                    facultyDto.Add(new FacultyDto
                    {
                        Id = item.Id,
                        Name = item.Name,
                        City = item.City,
                        Address = item.Address
                    });
                }
            }

            return facultyDto;
        }

        public bool Save(FacultyDto facultyDto)
        {
            Faculty faculty = new Faculty
            {
                Name = facultyDto.Name,
                City = facultyDto.City,
                Address =facultyDto.Address
            };

            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    unitOfWork.FacultyReposiotry.Insert(faculty);
                    unitOfWork.Save();
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Faculty faculty = unitOfWork.FacultyReposiotry.GetByID(id);
                    unitOfWork.FacultyReposiotry.Delete(faculty);
                    unitOfWork.Save();
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        public FacultyDto GetById(int id)
        {
            FacultyDto facultyDto = new FacultyDto();

            using (UnitOfWork unitOfWork = new UnitOfWork())
            {
                Faculty faculty = unitOfWork.FacultyReposiotry.GetByID(id);
                facultyDto = new FacultyDto
                {
                    Id = faculty.Id,
                    Name = faculty.Name,
                    Address = faculty.Address,
                    City = faculty.City
                };
            }

            return facultyDto;
        }

        public bool Update(FacultyDto facultyDto)
        {
            Faculty type = new Faculty
            {
                Id = facultyDto.Id,
                Name = facultyDto.Name,
                Address = facultyDto.Address,
                City = facultyDto.City
            };

            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    unitOfWork.FacultyReposiotry.Update(type);
                    unitOfWork.Save();
                }

                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}