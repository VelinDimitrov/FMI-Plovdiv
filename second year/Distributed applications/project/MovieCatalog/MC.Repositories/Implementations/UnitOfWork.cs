﻿using MC.Data.Contexts;
using MC.Data.Entities;
using System;

namespace MC.Repositories.Implementations
{
    public class UnitOfWork : IDisposable
    {
        private TermProjectDbContext context = new TermProjectDbContext();
        private GenericRepository<Student> studentRepository;
        private GenericRepository<Faculty> facultyReposiotry;
        private GenericRepository<Nationality> nationalityReposiotry;

        public GenericRepository<Student> StudentRepository
        {
            get
            {

                if (this.studentRepository == null)
                {
                    this.studentRepository = new GenericRepository<Student>(context);
                }
                return studentRepository;
            }
        }

        public GenericRepository<Faculty> FacultyReposiotry
        {
            get
            {
                if (this.facultyReposiotry == null)
                {
                    this.facultyReposiotry = new GenericRepository<Faculty>(context);
                }
                return facultyReposiotry;
            }
        }

        public GenericRepository<Nationality> NationalityReposiotry
        {
            get
            {
                if (this.nationalityReposiotry == null)
                {
                    this.nationalityReposiotry = new GenericRepository<Nationality>(context);
                }
                return nationalityReposiotry;
            }
        }

        public void Save()
        {
            context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
