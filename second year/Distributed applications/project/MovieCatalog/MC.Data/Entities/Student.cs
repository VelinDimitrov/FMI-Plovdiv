﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MC.Data.Entities
{
    public class Student : BaseEntity
    {
        [Required]
        [MaxLength(100)]
        [MinLength(1)]
        public string FirstName { get; set; }
        [Required]
        [MaxLength(100)]
        [MinLength(1)]
        public string LastName { get; set; }
        public DateTime? DateOfBirth { get; set; }

        [MaxLength(500)]
        [MinLength(1)]
        public string Comment { get; set; }

        [Required]
        public int NationalityId { get; set; }
        public virtual Nationality Nationality { get; set; }
        [Required]
        public int FacultyId { get; set; }
        public virtual Faculty Faculty { get; set; }
    }
}
