﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MC.Data.Contexts;
using MC.Data.Entities;
using MC.View.ViewModels;

namespace MC.View.Controllers
{
    public class NationalitiesController : Controller
    {
        private TermProjectDbContext db = new TermProjectDbContext();

        // GET: Nationalities
        public ActionResult Index()
        {
            List<NationalityVM> NationalityVMs = new List<NationalityVM>();
            using (NationalityReference.NationalityClient service = new NationalityReference.NationalityClient())
            {
                foreach (var item in service.GetNationalities())
                {
                    NationalityVMs.Add(new NationalityVM(item));
                }
            }
            return View(NationalityVMs);
        }

        // GET: Nationalities/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            using (NationalityReference.NationalityClient service = new NationalityReference.NationalityClient())
            {
                return View(new NationalityVM(service.GetNationalityByID(id.Value)));
            }
        }

        // GET: Nationalities/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Nationalities/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind] NationalityVM nationality)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    using (NationalityReference.NationalityClient service = new NationalityReference.NationalityClient())
                    {
                        NationalityReference.NationalityDto nationalityDto = new NationalityReference.NationalityDto
                        {                            
                            Name = nationality.Name,
                            Id = nationality.Id
                        };
                        service.PostNationality(nationalityDto);
                    }

                    return RedirectToAction("Index");
                }

                return View();
            }
            catch
            {
                return View();
            }
        }

        // GET: Nationalities/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            using (NationalityReference.NationalityClient service = new NationalityReference.NationalityClient())
            {
                return View(new NationalityVM(service.GetNationalityByID(id.Value)));
            }
        }

        // POST: Nationalities/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind] NationalityVM nationality)
        {
            if (ModelState.IsValid)
            {
                using (NationalityReference.NationalityClient service = new NationalityReference.NationalityClient())
                {
                    NationalityReference.NationalityDto nationalityDto = new NationalityReference.NationalityDto
                    {                        
                        Name = nationality.Name,
                        Id = nationality.Id
                    };
                    service.PutNationality(nationalityDto);
                }

                return RedirectToAction("Index");
            }
            return View(nationality);
        }

        // GET: Nationalities/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            using (NationalityReference.NationalityClient service = new NationalityReference.NationalityClient())
            {
                service.DeleteNationality(id.Value);
            }
            return RedirectToAction("Index");
        }

        // POST: Nationalities/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            using (NationalityReference.NationalityClient service = new NationalityReference.NationalityClient())
            {
                service.DeleteNationality(id);
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
