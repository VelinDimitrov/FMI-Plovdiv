﻿using MC.View.StudentReference;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MC.View.ViewModels
{
    public class StudentVM
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        [Display(Name = "Release date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true,ConvertEmptyStringToNull =true)]
        public DateTime DateOfBirth { get; set; }
        public string Comment { get; set; }

        public int FacultyId { get; set; }
        public FacultyVM faculty { get; set; }

        public int NationalityId { get; set; }
        public NationalityVM nationality { get; set; }

        public StudentVM() { }

        public StudentVM(StudentDto studentDto)
        {
            Id = studentDto.Id;
            FirstName = studentDto.FirstName;
            LastName = studentDto.LastName;
            DateOfBirth = studentDto.DateOfBirth.Value;
            Comment = studentDto.Comment;
            faculty = new FacultyVM
            {
                Id = studentDto.faculty.Id,
                Name = studentDto.faculty.Name,
                Address = studentDto.faculty.Address,
                City = studentDto.faculty.City
            };
            FacultyId = studentDto.faculty.Id;
            nationality = new NationalityVM
            {
                Id = studentDto.nationality.Id,
                Name = studentDto.nationality.Name
            };
            NationalityId = studentDto.nationality.Id;
        }
    }
}