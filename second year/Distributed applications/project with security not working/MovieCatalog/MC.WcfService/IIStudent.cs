﻿using MC.ApplicationServices.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace MC.WcfServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IIStudent" in both code and config file together.
    [ServiceContract]
    public interface IIStudent
    {
        [OperationContract]
        List<StudentDto> GetStudents();

        [OperationContract]
        string PostStudent(StudentDto studentDto);

        [OperationContract]
        string PutStudent(StudentDto studentDto);

        [OperationContract]
        string DeleteStudents(int id);

        [OperationContract]
        StudentDto GetStudentByID(int id);
    }
}
