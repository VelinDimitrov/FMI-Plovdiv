﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using MC.ApplicationServices.DTOs;
using MC.ApplicationServices.Implementations;

namespace MC.WcfServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Nationality" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Nationality.svc or Nationality.svc.cs at the Solution Explorer and start debugging.
    public class Nationality : INationality
    {
        #region Fields
        private NationalityManagementService service = new NationalityManagementService();
        #endregion

        public string DeleteNationality(int id)
        {
            if (!service.Delete(id))
                return "Nationality is not deleted";

            return "Nationality is deleted";
        }

        public List<NationalityDto> GetNationalities()
        {
            return service.Get();
        }

        public NationalityDto GetNationalityByID(int id)
        {
            return service.GetById(id);
        }

        public string PostNationality(NationalityDto nationalityDto)
        {
            if (!service.Save(nationalityDto))
                return "Nationality is not inserted";

            return "Nationality is inserted";
        }

        public string PutNationality(NationalityDto nationalityDto)
        {
            if (!service.Update(nationalityDto))
                return "Nationality is not updated";

            return "Nationality is updated";
        }
    }
}
