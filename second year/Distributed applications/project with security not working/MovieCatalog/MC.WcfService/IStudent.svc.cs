﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.ServiceModel;
using System.Text;
using MC.ApplicationServices.DTOs;
using MC.ApplicationServices.Implementations;

namespace MC.WcfServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "IStudent" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select IStudent.svc or IStudent.svc.cs at the Solution Explorer and start debugging.
    public class IStudent : IIStudent
    {
        #region Fields
        private StudentManagementService service = new StudentManagementService();
        #endregion

        public string DeleteStudents(int id)
        {
            if (!service.Delete(id))
                return "Student is not deleted";

            return "Student is deleted";
        }

        public StudentDto GetStudentByID(int id)
        {
            return service.GetById(id);
        }

        [PrincipalPermission(SecurityAction.Demand, Role = "ADMIN")]
        public List<StudentDto> GetStudents()
        {
            return service.Get();
        }

        public string PostStudent(StudentDto studentDto)
        {
            if (!service.Save(studentDto))
                return "Student is not inserted";

            return "Student is inserted";
        }

        public string PutStudent(StudentDto studentDto)
        {
            if (!service.Update(studentDto))
                return "Student is not updated";

            return "Student is updated";
        }
    }
}
