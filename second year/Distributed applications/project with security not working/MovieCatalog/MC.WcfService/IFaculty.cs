﻿using MC.ApplicationServices.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace MC.WcfServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IFaculty" in both code and config file together.
    [ServiceContract]
    public interface IFaculty
    {
        [OperationContract]
        List<FacultyDto> GetFaculties();

        [OperationContract]
        string PostFaculty(FacultyDto facultyDto);

        [OperationContract]
        string PutFaculty(FacultyDto facultyDto);

        [OperationContract]
        string DeleteFaculty(int id);

        [OperationContract]
        FacultyDto GetFacultyByID(int id);
    }
}
