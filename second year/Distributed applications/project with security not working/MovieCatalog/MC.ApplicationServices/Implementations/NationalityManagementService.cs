﻿using MC.ApplicationServices.DTOs;
using MC.Data.Entities;
using MC.Repositories.Implementations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MC.ApplicationServices.Implementations
{
    public class NationalityManagementService
    {
        public List<NationalityDto> Get()
        {
            List<NationalityDto> nationalityDto = new List<NationalityDto>();

            using (UnitOfWork unitOfWork = new UnitOfWork())
            {
                foreach (var item in unitOfWork.NationalityReposiotry.Get())
                {
                    nationalityDto.Add(new NationalityDto
                    {
                        Id = item.Id,
                        Name = item.Name
                    });
                }
            }

            return nationalityDto;
        }

        public bool Save(NationalityDto nationalityDto)
        {
            Nationality nationality = new Nationality
            {
                Name = nationalityDto.Name
            };

            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    unitOfWork.NationalityReposiotry.Insert(nationality);
                    unitOfWork.Save();
                }

                return true;
            }
            catch(Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Nationality nationality = unitOfWork.NationalityReposiotry.GetByID(id);
                    unitOfWork.NationalityReposiotry.Delete(nationality);
                    unitOfWork.Save();
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        public NationalityDto GetById(int id)
        {
            NationalityDto nationalityDto = new NationalityDto();

            using (UnitOfWork unitOfWork = new UnitOfWork())
            {
                Nationality faculty = unitOfWork.NationalityReposiotry.GetByID(id);
                nationalityDto = new NationalityDto
                {
                    Id = faculty.Id,
                    Name = faculty.Name                    
                };
            }

            return nationalityDto;
        }

        public bool Update(NationalityDto nationalityDto)
        {
            Nationality nationality = new Nationality
            {
                Id = nationalityDto.Id,
                Name = nationalityDto.Name
            };

            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    unitOfWork.NationalityReposiotry.Update(nationality);
                    unitOfWork.Save();
                }

                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}