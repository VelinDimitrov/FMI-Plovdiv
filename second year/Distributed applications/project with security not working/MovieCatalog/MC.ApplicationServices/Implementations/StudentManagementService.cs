﻿using MC.ApplicationServices.DTOs;
using MC.Data.Entities;
using MC.Repositories.Implementations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MC.ApplicationServices.Implementations
{
    public class StudentManagementService
    {
        public List<StudentDto> Get()
        {
            List<StudentDto> moviesDto = new List<StudentDto>();

            using (UnitOfWork unitOfWork = new UnitOfWork())
            {
                foreach (var item in unitOfWork.StudentRepository.Get())
                {
                    moviesDto.Add(new StudentDto
                    {
                        Id = item.Id,
                        FirstName =item.FirstName,
                        LastName = item.LastName,
                        Comment = item.Comment,
                        DateOfBirth = item.DateOfBirth,
                        faculty = new FacultyDto
                        {
                            Name = item.Faculty.Name,
                            Address = item .Faculty.Address,
                            City = item.Faculty.City
                        },
                        nationality = new NationalityDto
                        {
                            Name = item.Nationality.Name
                        }
                    });
                }
            }

            return moviesDto;
        }

        public bool Save(StudentDto studentDto)
        {


            using (UnitOfWork unitOfWork = new UnitOfWork())
            {
                Student movie = new Student
                {
                    FirstName = studentDto.FirstName,
                    LastName = studentDto.LastName,
                    Comment = studentDto.Comment,
                    DateOfBirth = studentDto.DateOfBirth,                   
                    Faculty = unitOfWork.FacultyReposiotry.GetByID(studentDto.faculty.Id),
                    Nationality = unitOfWork.NationalityReposiotry.GetByID(studentDto.nationality.Id)
                };

                unitOfWork.StudentRepository.Insert(movie);
                unitOfWork.Save();
            }

            return true;

        }

        public bool Delete(int id)
        {

            using (UnitOfWork unitOfWork = new UnitOfWork())
            {
                Student student = unitOfWork.StudentRepository.GetByID(id);
                unitOfWork.StudentRepository.Delete(student);
                unitOfWork.Save();
            }

            return true;

        }

        public StudentDto GetById(int id)
        {
            StudentDto studentDto = new StudentDto();

            using (UnitOfWork unitOfWork = new UnitOfWork())
            {
                Student student = unitOfWork.StudentRepository.GetByID(id);
                studentDto = new StudentDto
                {
                    Id = student.Id,
                    FirstName = student.FirstName,
                    LastName = student.LastName,
                    Comment = student.Comment,
                    DateOfBirth = student.DateOfBirth,
                    faculty = new FacultyDto
                    {
                        Id = student.Faculty.Id,
                        Name = student.Faculty.Name,
                        Address = student.Faculty.Address,
                        City = student.Faculty.City
                    },
                    nationality = new NationalityDto
                    {
                        Id = student.Nationality.Id,
                        Name = student.Nationality.Name
                    }
                };
            }

            return studentDto;
        }

        public bool Update(StudentDto studentDto)
        {

            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Student student = new Student
                    {
                        Id = studentDto.Id,
                        FirstName = studentDto.FirstName,
                        LastName = studentDto.LastName,
                        Comment = studentDto.Comment,
                        DateOfBirth = studentDto.DateOfBirth,
                        FacultyId = studentDto.faculty.Id,
                        Faculty = unitOfWork.FacultyReposiotry.GetByID(studentDto.faculty.Id),
                        NationalityId = studentDto.nationality.Id,
                        Nationality = unitOfWork.NationalityReposiotry.GetByID(studentDto.nationality.Id)
                    };
                    unitOfWork.StudentRepository.Update(student);
                    unitOfWork.Save();
                }

                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}