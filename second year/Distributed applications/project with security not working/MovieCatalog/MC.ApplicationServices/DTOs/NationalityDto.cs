﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MC.ApplicationServices.DTOs
{
    public class NationalityDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}