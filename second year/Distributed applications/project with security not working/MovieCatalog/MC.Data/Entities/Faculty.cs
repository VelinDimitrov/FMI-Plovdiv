﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MC.Data.Entities
{
    public class Faculty : BaseEntity
    {
        [Required]
        [MaxLength(200)]
        [MinLength(1)]
        public string Name { get; set; }
        [Required]
        public string City { get; set; }
        
        [MaxLength(200)]
        [MinLength(1)]
        public string Address { get; set; }
    }
}
