import axios from "axios";
import { getAllTasksByUserId, deleteTask } from "./taskApi";

const apiUrl = "http://localhost:3005";

export function getAllUsers() {
  return axios.get(`${apiUrl}/users`);
}

export function getUserById(id) {
  return axios.get(`${apiUrl}/users/${id}`).then((response) => response.data);
}

export function getLoggedUser() {
  return JSON.parse(localStorage.getItem("loggedUser"));
}

export function requiresAdminOrError() {
  const loggedUser = getLoggedUser();
  if (!loggedUser.isAdmin) {
    throw new Error("Logged user is not admin");
  }
}

export async function register(userData) {
  const users = (await getAllUsers()).data;

  if (users.find((u) => u.email === userData.email)) {
    throw new Error("Email already exists!");
  }

  userData = {
    ...userData,
    isAdmin: false,
  };
  return axios.post(`${apiUrl}/users`, userData);
}

export async function login(userData) {
  const users = (await getAllUsers()).data;

  const loggedUser = users.find(
    (u) =>
      u.email === userData.email && u.password.toString() === userData.password
  );

  if (loggedUser) {
    localStorage.setItem("loggedUser", JSON.stringify(loggedUser));
    return;
  }

  throw new Error("Incorrect e-mail/password");
}

export function logout() {
  localStorage.removeItem("loggedUser");
}

export async function saveUser(userData) {
  requiresAdminOrError();
  const users = (await getAllUsers()).data;

  if (users.find((u) => u.email === userData.email && u.id !== userData.id)) {
    throw new Error("Email already exists!");
  }

  if (userData.id) {
    requiresAdminOrError();
    return axios.put(`${apiUrl}/users/${userData.id}`, userData);
  }

  return axios.post(`${apiUrl}/users`, userData);
}

export function deleteUser(id) {
  requiresAdminOrError();
  getAllTasksByUserId(id).then((task) => deleteTask(task.id));

  return axios.delete(`${apiUrl}/users/${id}`);
}
