import axios from "axios";
import { ALL_TASK_STATUSES } from "../components/tasks/taskStatus";
import toastr from "toastr";
import { getLoggedUser } from "./userApi";

const apiUrl = "http://localhost:3005";

export function getAllTasks() {
  return axios.get(`${apiUrl}/tasks`);
}

export function getTaskById(id) {
  return axios.get(`${apiUrl}/tasks/${id}`).then((response) => response.data);
}

const validate = (taskData) => {
  let errorMessages = "";
  if (!taskData.evaluation) {
    errorMessages += "Evaluation is required!\n";
  }
  if (!taskData.title) {
    errorMessages += "Title is required!\n";
  }
  if (!taskData.description) {
    errorMessages += "Description is required!\n";
  }
  if (!ALL_TASK_STATUSES.includes(taskData.status)) {
    errorMessages += "Status is required!\n";
  }
  return errorMessages;
};

export function saveTask(taskData) {
  const errorMessages = validate(taskData);
  if (errorMessages) {
    throw new Error(errorMessages);
  }
  if (taskData.id) {
    const loggedUser = getLoggedUser();
    if (!loggedUser.isAdmin && taskData.userId !== loggedUser.id) {
      toastr.error("You do not have privelage to edit task!");
      return;
    }
    return axios.put(`${apiUrl}/tasks/${taskData.id}`, taskData);
  }

  return axios.post(`${apiUrl}/tasks`, taskData);
}

export function deleteTask(id) {
  return axios.delete(`${apiUrl}/tasks/${id}`);
}

export async function getAllTasksByUserId(id) {
  const tasks = (await getAllTasks()).data;

  return tasks.filter((task) => task && task.userId === id);
}
