import React from "react";
import { Switch, Route } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import Home from "./components/home/Home";
import NavigationBar from "./components/nav/NavigationBar";
import Login from "./components/authentication/Login";
import Register from "./components/authentication/Register";
import TasksList from "./components/tasks/TasksList";
import UsersList from "./components/users/UsersList";
import TaskForm from "./components/tasks/TaskForm";
import UserForm from "./components/users/UserForm";
import AuthenticationComponent from "./components/authentication/AuthenticationComponent";
import "./App.css";

function App() {
  return (
    <>
      <NavigationBar />
      <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/login" component={Login} />
        <Route exact path="/register" component={Register} />
        <AuthenticationComponent exact path="/tasks" component={TasksList} />
        <AuthenticationComponent
          exact
          path="/tasks/create"
          component={TaskForm}
        />
        <AuthenticationComponent
          exact
          path="/tasks/:taskId/edit"
          component={TaskForm}
        />
        <AuthenticationComponent
          exact
          path="/users"
          admin
          component={UsersList}
        />
        <AuthenticationComponent
          exact
          path="/users/create"
          admin
          component={UserForm}
        />
        <AuthenticationComponent
          exact
          path="/users/:id/edit"
          admin
          component={UserForm}
        />
      </Switch>
    </>
  );
}

export default App;
