import React from "react";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import { getLoggedUser, logout } from "../../core/userApi";
import { withRouter } from "react-router-dom";

const NavigationBar = (props) => {
  const loggedUser = getLoggedUser();
  return (
    <Navbar bg="dark" variant="dark" style={{ marginBottom: "1rem" }}>
      <Navbar.Brand href="/">My React app</Navbar.Brand>
      <Nav className="mr-auto">
        {loggedUser && loggedUser.isAdmin && (
          <Nav.Link href="/users">Users</Nav.Link>
        )}
        <Nav.Link href="/tasks">Tasks</Nav.Link>
      </Nav>
      <Navbar.Collapse className="justify-content-end">
        {loggedUser ? (
          <Navbar.Text>
            Signed in as:{" "}
            <b>
              {loggedUser.firstName} {loggedUser.lastName}
            </b>{" "}
            (
            <span
              className="button-link"
              onClick={() => {
                logout();
                props.history.push("/");
              }}
            >
              Logout
            </span>
            )
          </Navbar.Text>
        ) : (
          <Navbar.Text>
            <a href="/login">Login</a>/<a href="/register">Register</a>
          </Navbar.Text>
        )}
      </Navbar.Collapse>
    </Navbar>
  );
};

export default withRouter(NavigationBar);
