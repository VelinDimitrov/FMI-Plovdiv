import React, { useState, useEffect } from "react";
import { Row, Col, Form, Button } from "react-bootstrap";
import { getUserById, saveUser } from "../../core/userApi";
import { withRouter } from "react-router";

const UserForm = (props) => {
  let [errorMessage, setErrorMessage] = useState("");
  let [user, setUser] = useState({});

  useEffect(() => {
    let userId = props.computedMatch.params.id;
    if (userId) {
      setUser(getUserById(userId).then((user) => setUser(user)));
    }
  }, [props.computedMatch.params.id]);

  const handleChange = (e) => {
    setUser({ ...user, [e.target.name]: e.target.value }, setErrorMessage(""));
  };

  const handleIsAdminChange = (e) => {
    setUser({ ...user, isAdmin: e.target.checked });
  };

  let { id, firstName, lastName, isAdmin, email, password } = user;
  return (
    <>
      <h1 style={{ textAlign: "center" }}>{id ? "Edit" : "Create user"}</h1>
      {errorMessage && (
        <h1 style={{ textAlign: "center", color: "red" }}>{errorMessage}</h1>
      )}
      <Row>
        <Col md={4}></Col>
        <Col md={4}>
          <Form>
            {id && <Form.Control type="hidden" value={id} />}

            <Form.Group controlId="formFirstName">
              <Form.Label>First name</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter first name"
                name="firstName"
                value={firstName}
                onChange={handleChange}
              />
            </Form.Group>

            <Form.Group controlId="formLastName">
              <Form.Label>Last name</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter last name"
                name="lastName"
                value={lastName}
                onChange={handleChange}
              />
            </Form.Group>

            <Form.Group controlId="formBasicEmail">
              <Form.Label>Email address</Form.Label>
              <Form.Control
                type="email"
                placeholder="Enter email"
                name="email"
                value={email}
                onChange={handleChange}
              />
            </Form.Group>

            <Form.Group controlId="formBasicPassword">
              <Form.Label>Password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Password"
                name="password"
                value={password}
                onChange={handleChange}
              />
            </Form.Group>

            {id && (
              <Form.Group controlId="formBasicPassword">
                <Form.Check
                  type="checkbox"
                  label="Admin"
                  checked={isAdmin}
                  onChange={handleIsAdminChange}
                />
              </Form.Group>
            )}

            <Button
              variant="primary"
              onClick={() =>
                saveUser(user)
                  .then(() => props.history.push("/users"))
                  .catch((err) => setErrorMessage(err.message))
              }
            >
              Save
            </Button>
          </Form>
        </Col>
        <Col md={4}></Col>
      </Row>
    </>
  );
};

export default withRouter(UserForm);
