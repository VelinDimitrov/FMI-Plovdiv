import React, { useState, useEffect } from "react";
import { Col, Row, Button } from "react-bootstrap";
import UserCard from "./UserCard";
import { getAllUsers, deleteUser } from "../../core/userApi";

const UsersList = ({ loggedUser }) => {
  const [users, setUsers] = useState([]);

  useEffect(() => {
    const getUsers = async () => {
      getAllUsers().then((allUsers) =>
        setUsers(allUsers.data.filter((u) => u.id !== loggedUser.id))
      );
    };
    getUsers();
  }, [loggedUser]);

  const onUserDelete = (id) => {
    deleteUser(id)
      .then(() => {
        setUsers((prevState) => {
          return prevState.filter((u) => u.id !== id);
        });
      })
      .catch((err) => console.error(err));
  };

  return (
    <>
      <Row>
        {users.map((user) => (
          <Col md={3}>
            <UserCard
              user={user}
              key={user.id}
              onUserDelete={onUserDelete}
              loggedUser={loggedUser}
            />
          </Col>
        ))}
      </Row>
      <Button variant="primary" href="/users/create">
        Create User
      </Button>
    </>
  );
};

export default UsersList;
