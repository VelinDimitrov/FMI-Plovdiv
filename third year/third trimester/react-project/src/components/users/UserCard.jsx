import React from "react";
import { Card, Form, Button } from "react-bootstrap";

const UserCard = ({
  user: { id, firstName, lastName, isAdmin, email },
  onUserDelete,
  loggedUser,
}) => {
  return (
    <Card style={{ width: "18rem", marginBottom: "1rem" }}>
      <Card.Body>
        <Card.Title>
          {firstName} {lastName}
        </Card.Title>
        <Card.Text>Email : {email}</Card.Text>
        <Form.Check type="checkbox" disabled label="Admin" checked={isAdmin} />

        {loggedUser && loggedUser.isAdmin && (
          <>
            <Button href={`/users/${id}/edit`} variant="link">
              Edit
            </Button>
            <Button variant="link" onClick={() => onUserDelete(id)}>
              Delete
            </Button>
          </>
        )}
      </Card.Body>
    </Card>
  );
};

export default UserCard;
