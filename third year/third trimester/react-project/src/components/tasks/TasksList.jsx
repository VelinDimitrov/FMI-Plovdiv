import React, { useState, useEffect } from "react";
import { Col, Row, Button } from "react-bootstrap";
import {
  getAllTasks,
  deleteTask,
  getAllTasksByUserId,
} from "../../core/taskApi";
import TaskCard from "./TaskCard";
import toastr from "toastr";

const TasksList = ({ loggedUser }) => {
  const [tasks, setTasks] = useState([]);

  useEffect(() => {
    if (loggedUser && !loggedUser.isAdmin) {
      getAllTasksByUserId(loggedUser.id).then((response) => setTasks(response));
    } else {
      getAllTasks().then((response) => setTasks(response.data));
    }
  }, [loggedUser]);

  const onTaskDelete = (id, userId) => {
    if (!loggedUser.isAdmin && userId !== loggedUser.id) {
      toastr.error("You do not have privelage to delete task!");
      return;
    }
    deleteTask(id)
      .then(() => {
        setTasks((prevState) => {
          return prevState.filter((u) => u.id !== id);
        });
      })
      .catch((err) => console.error(err));
  };

  return (
    <>
      <Row>
        {tasks && tasks.length > 0 ? (
          tasks.map((task) => (
            <Col md={3} key={task.id}>
              <TaskCard
                task={task}
                onTaskDelete={(id) => onTaskDelete(id, task.userId)}
              />
            </Col>
          ))
        ) : (
          <Col md={12}>
            <h1 style={{ textAlign: "center" }}>No tasks for user</h1>
          </Col>
        )}
      </Row>
      <Button variant="primary" href="/tasks/create">
        Create Task
      </Button>
    </>
  );
};

export default TasksList;
