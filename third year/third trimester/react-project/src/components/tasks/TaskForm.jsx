import React from "react";
import { withRouter } from "react-router";
import { Row, Col, Form, Button } from "react-bootstrap";
import { getTaskById, saveTask } from "../../core/taskApi";
import { ALL_TASK_STATUSES } from "./taskStatus";
import { getLoggedUser } from "../../core/userApi";

class TaskForm extends React.Component {
  state = {
    task: {},
    errorMessage: "",
  };

  componentDidMount() {
    let userId = getLoggedUser().id;
    let taskId = this.props.computedMatch.params.taskId;
    if (taskId) {
      getTaskById(taskId).then((task) => this.setState({ task: task }));
    } else {
      this.setState({ task: { userId: userId } });
    }
  }

  handleChange = (e) => {
    e.persist();
    this.setState((prevState) => ({
      task: { ...prevState.task, [e.target.name]: e.target.value },
      errorMessage: "",
    }));
  };

  render() {
    let {
      task: { id, title, description, status, evaluation },
      errorMessage,
    } = this.state;
    return (
      <>
        <h1 style={{ textAlign: "center" }}>{id ? "Edit" : "Create task"}</h1>
        {errorMessage && (
          <h3 style={{ textAlign: "center", color: "red" }}>{errorMessage}</h3>
        )}
        <Row>
          <Col md={4}></Col>
          <Col md={4}>
            <Form>
              {id && <Form.Control type="hidden" value={id} />}

              <Form.Group>
                <Form.Label>Title</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter title"
                  name="title"
                  value={title}
                  onChange={this.handleChange}
                />
              </Form.Group>

              <Form.Group>
                <Form.Label>Description</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter description"
                  name="description"
                  value={description}
                  onChange={this.handleChange}
                />
              </Form.Group>

              <Form.Group controlId="formGridState">
                <Form.Label>Status</Form.Label>
                <Form.Control
                  as="select"
                  value={status}
                  onChange={this.handleChange}
                  name="status"
                >
                  <option>Choose...</option>
                  {ALL_TASK_STATUSES.map((status) => (
                    <option>{status}</option>
                  ))}
                </Form.Control>
              </Form.Group>

              <Form.Group>
                <Form.Label>Evaluation</Form.Label>
                <Form.Control
                  type="number"
                  placeholder="Enter evaluation"
                  name="evaluation"
                  value={evaluation}
                  onChange={this.handleChange}
                />
              </Form.Group>
              <Button
                variant="primary"
                onClick={() => {
                  try {
                    saveTask(this.state.task).then(() =>
                      this.props.history.push("/tasks")
                    );
                  } catch (error) {
                    this.setState({ errorMessage: error.message });
                  }
                }}
              >
                Save
              </Button>
            </Form>
          </Col>
          <Col md={4}></Col>
        </Row>
      </>
    );
  }
}

export default withRouter(TaskForm);
