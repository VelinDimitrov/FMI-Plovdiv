import React, { useState, useEffect } from "react";
import { Card, Button } from "react-bootstrap";
import { getUserById } from "../../core/userApi";

const TaskCard = ({
  task: { id, title, description, status, evaluation, userId },
  onTaskDelete,
}) => {
  const [taskUser, setTaskUser] = useState({});

  useEffect(() => {
    getUserById(userId).then((response) => setTaskUser(response));
  }, [userId]);

  return (
    <Card style={{ width: "18rem", marginBottom: "1rem" }}>
      <Card.Body>
        <Card.Title>{title}</Card.Title>
        <Card.Text>{description}</Card.Text>
        <Card.Text>Status: {status}</Card.Text>
        <Card.Text>Evaluation: {evaluation}</Card.Text>
        <Card.Text>
          Creator: {taskUser.firstName} {taskUser.lastName}
        </Card.Text>
        <Button href={`/tasks/${id}/edit`} variant="link">
          Edit
        </Button>
        <Button variant="link" onClick={() => onTaskDelete(id)}>
          Delete
        </Button>
      </Card.Body>
    </Card>
  );
};

export default TaskCard;
