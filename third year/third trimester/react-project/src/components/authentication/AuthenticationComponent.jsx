import React from "react";
import { getLoggedUser } from "../../core/userApi";
import { Redirect } from "react-router-dom";
import toastr from "toastr";

const AuthenticationComponent = (props) => {
  const loggedUser = getLoggedUser();

  if (!props.admin && loggedUser) {
    return <props.component {...props} loggedUser={loggedUser} />;
  }

  if (props.admin && loggedUser && loggedUser.isAdmin) {
    return <props.component {...props} loggedUser={loggedUser} />;
  }

  if (props.admin && loggedUser && !loggedUser.isAdmin) {
    toastr.error("You do not have privelage to see this page!");
    return <Redirect to="/" />;
  }

  return <Redirect to="/login" />;
};

export default AuthenticationComponent;
