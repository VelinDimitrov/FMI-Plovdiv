import React, { useState } from "react";
import { Row, Col, Form, Button } from "react-bootstrap";
import { register, login } from "../../core/userApi";
import { withRouter } from "react-router";

const Register = (props) => {
  let [errorMessage, setErrorMessage] = useState("");
  let [user, setUser] = useState({});

  const handleChange = (e) => {
    setUser({ ...user, [e.target.name]: e.target.value }, setErrorMessage(""));
  };

  let { firstName, lastName, email } = user;
  return (
    <>
      <h1 style={{ textAlign: "center" }}>Register</h1>
      {errorMessage && (
        <h1 style={{ textAlign: "center", color: "red" }}>{errorMessage}</h1>
      )}
      <Row>
        <Col md={4}></Col>
        <Col md={4}>
          <Form>
            <Form.Group controlId="formFirstName">
              <Form.Label>First name</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter first name"
                name="firstName"
                value={firstName}
                onChange={handleChange}
              />
            </Form.Group>

            <Form.Group controlId="formLastName">
              <Form.Label>Last name</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter last name"
                name="lastName"
                value={lastName}
                onChange={handleChange}
              />
            </Form.Group>

            <Form.Group controlId="formBasicEmail">
              <Form.Label>Email address</Form.Label>
              <Form.Control
                type="email"
                placeholder="Enter email"
                name="email"
                value={email}
                onChange={handleChange}
              />
            </Form.Group>

            <Form.Group controlId="formBasicPassword">
              <Form.Label>Password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Password"
                name="password"
                onChange={handleChange}
              />
            </Form.Group>

            <Button
              variant="primary"
              onClick={() =>
                register(user)
                  .then((response) => login(response.data))
                  .then(() => props.history.push("/"))
                  .catch((err) => setErrorMessage(err.message))
              }
            >
              Register
            </Button>
          </Form>
        </Col>
        <Col md={4}></Col>
      </Row>
    </>
  );
};

export default withRouter(Register);
