import React, { useState } from "react";
import { withRouter } from "react-router";
import { Row, Col, Form, Button } from "react-bootstrap";
import { login } from "../../core/userApi";

const Login = (props) => {
  let [errorMessage, setErrorMessage] = useState("");
  let [user, setUser] = useState({});

  const handleChange = (e) => {
    setUser({ ...user, [e.target.name]: e.target.value }, setErrorMessage(""));
  };

  return (
    <>
      <h1 style={{ textAlign: "center" }}>Login</h1>
      {errorMessage && (
        <h1 style={{ textAlign: "center", color: "red" }}>{errorMessage}</h1>
      )}
      <Row>
        <Col md={4}></Col>
        <Col md={4}>
          <Form>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>Email address</Form.Label>
              <Form.Control
                type="email"
                placeholder="Enter email"
                value={user.email}
                name="email"
                onChange={handleChange}
              />
            </Form.Group>

            <Form.Group controlId="formBasicPassword">
              <Form.Label>Password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Password"
                value={user.password}
                name="password"
                onChange={handleChange}
              />
            </Form.Group>

            <Button
              variant="primary"
              onClick={() =>
                login(user)
                  .then(() => props.history.push("/"))
                  .catch((err) => setErrorMessage(err.message))
              }
            >
              Login
            </Button>
          </Form>
        </Col>
        <Col md={4}></Col>
      </Row>
    </>
  );
};

export default withRouter(Login);
