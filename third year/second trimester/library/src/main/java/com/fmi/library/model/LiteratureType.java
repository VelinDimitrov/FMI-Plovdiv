package com.fmi.library.model;

public enum LiteratureType {
	POETRY, DRAMA, PROSE, NONFICTION, MEDIA;
}
