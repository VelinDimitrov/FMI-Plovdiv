<?php

if (isset($_GET['action']) && $_GET['action'] == 'logout') {
    logout();
}

function logout()
{
    if (isset($_SESSION['username'])) {
        unset($_SESSION['username']);
        session_write_close();
    }
    header('Location: /php/index.php');
}

?>

<head>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="/php">Navbar</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse">
            <ul class="navbar-nav mr-auto">
                <?php if (isset($_SESSION['username'])) { ?>
                    <li class="nav-item">
                        <a class="nav-link" href="/php/main/tasks.php">Tasks</a>
                    </li>
                <?php } ?>

            </ul>

            <ul class="navbar-nav">
                <?php if (!isset($_SESSION['username'])) { ?>
                    <li class="nav-item">
                        <a class="nav-link" href="/php/main/login.php">Login</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/php/main/register.php">Register</a>
                    </li>
                <?php } ?>

                <?php if (isset($_SESSION['username'])) { ?>
                    Здравей, <?= $_SESSION['username'] ?>
                    <li class="nav-item">
                        <a class="nav-link" href='?action=logout'>Изход</a>
                    </li>
                <?php } ?>
            </ul>



        </div>

    </nav>