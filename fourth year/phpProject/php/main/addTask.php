<?php
session_start();

require_once('../MyDB.php');
$conn = DbHelper::GetConnection();
$id = -1;
$task = null;
$error = "";

if (isset($_GET['id'])) {
    $id = $_GET['id'];
    $stm = $conn->prepare("SELECT * FROM TASKS WHERE TASK_ID = ?");
    $stm->execute(array($id));
    $tasks = $stm->fetchAll(PDO::FETCH_ASSOC);
    if (count($tasks)) {
        $task = $tasks[0];
    }
}



if (isset($_POST['create'])) {
    createOrUpdate();
}


function createOrUpdate()
{
    if (isset($_POST['title']) && $_POST['title'] != '') {

        if (strlen($_POST['title'] > 200)) {
            $GLOBALS["error"] = "Максималната дължина на заглавието е 200";
        } else {
            if ($GLOBALS['task'] != null) {
                $stm = $GLOBALS['conn']->prepare('UPDATE tasks SET TITLE=?, CONTENT=?, APPOINTED_DATE=?, APPOINTED_TIME=? WHERE TASK_ID=?;');
                $stm->execute(array($_POST['title'], $_POST['content'], $_POST['appointedDate'], $_POST['appointedTime'], $GLOBALS['id']));
            } else {
                $stm = $GLOBALS['conn']->prepare('INSERT INTO TASKS(TITLE, CONTENT, APPOINTED_DATE, APPOINTED_TIME) VALUES(?, ?, ?, ?);');
                $stm->execute(array($_POST['title'], $_POST['content'], $_POST['appointedDate'], $_POST['appointedTime']));
            }

            header('Location: /php/main/tasks.php');
        }
    } else {
        $GLOBALS["error"] = "Заглавието е задължително";
    }
}
?>


<!DOCTYPE html>
<html>

<?php require('../header/nav.php') ?>
<div class="row">
    <div class="col-md-4"></div>
    <div class="col-md-4">
        <h2>Task <?= ($task != null) ? 'Update' : 'Create' ?></h2>

        <?php echo "<div class='error' style='color:red'>$error</div>"; ?>
        <form method="POST">
            <div class="form-group">
                <label for="inputUsername">Title</label>
                <input type="text" class="form-control" id="inputUsername" name="title" value="<?= ($task != null) ? $task["TITLE"] : "" ?>">
            </div>
            <div class="form-group">
                <label for="inputContent">Content</label>
                <input type="text" class="form-control" id="inputContent" name="content" value="<?= ($task != null) ? $task["CONTENT"] : "" ?>">
            </div>
            <div class="form-group">
                <label for="inputDate">Date</label>
                <input type="date" class="form-control" id="inputDate" name="appointedDate" value="<?= ($task != null) ? $task["APPOINTED_DATE"] : "" ?>">
            </div>
            <div class="form-group">
                <label for="inputTime">Time</label>
                <input type="time" class="form-control" id="inputTime" name="appointedTime" value="<?= ($task != null) ? $task["APPOINTED_TIME"] : "" ?>">
            </div>
            <button type="submit" name="create" class="btn btn-primary"> <?= ($task != null) ? 'Update' : 'Create' ?></button>
        </form>
    </div>
</div>


</body>

</html>