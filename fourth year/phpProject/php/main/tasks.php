<?php
session_start();
require_once('../MyDB.php');

$conn = DbHelper::GetConnection();
$stm = $conn->query("SELECT * FROM tasks");
$rows = $stm->fetchAll(PDO::FETCH_ASSOC);
?>
<!DOCTYPE html>
<html>

<?php require('../header/nav.php') ?>

<table class="table">
    <tr>
        <th scope="col">Title</th>
        <th scope="col">Operations</th>
    </tr>
    <?php
    foreach ($rows as $r) {
    ?>
        <tr>
            <td><?= $r["TITLE"] ?></td>
            <td>
                <a href="addTask.php?id=<?= $r["TASK_ID"] ?>">Edit</a>
                <a href="delete.php?id=<?= $r["TASK_ID"] ?>">Delete</a>
            </td>
        </tr>
    <?php
    }
    ?>
</table>
<a class="btn btn-primary" href="addTask.php" role="button">Create New</a>

</body>

</html>