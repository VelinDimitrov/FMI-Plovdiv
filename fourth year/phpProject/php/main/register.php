<?php
require_once('../MyDB.php');
session_start();

$conn = DbHelper::GetConnection();
$error = "";

if (isset($_POST['register'])) {
    register();
}

function register()
{
    $errorMessage = validateUserName();
    $errorMessage .= validatePassword();
    $errorMessage .= validateConfirmPassword();

    if ($errorMessage) {
        $GLOBALS["error"] = $errorMessage;
    } else {
        $stm = $GLOBALS['conn']->prepare('INSERT INTO USERS(USERNAME, PASSWORD) VALUES(?, ?);');
        $stm->execute(array($_POST['username'], $_POST['password']));

        $_SESSION['username'] = $_POST['username'];
        header('Location: /php/main/tasks.php');
    }
}


function validatePassword()
{
    $errorMessage = "";
    if (isset($_POST['password']) && $_POST['password'] != '') {
        $password = $_POST['password'];

        if (!preg_match("#[0-9]{2,}#", $password)) {
            $errorMessage .= "Вашата парола трябва да съдържа поне 2 цифри<br/>";
        }

        if (!preg_match("#[_*&$]+#", $password)) {
            $errorMessage .= "Вашата парола трябва да съдържа поне 1 специален символ(_*&$)<br/>";
        }

        if (!preg_match("#[A-Z]+#", $password)) {
            $errorMessage .= "Вашата парола трябва да съдържа поне 1 главна буква<br/>";
        }

        if (!preg_match("#[a-z]+#", $password)) {
            $errorMessage .= "Вашата парола трябва да съдържа поне 1 малка буква<br/>";
        }
    } else {
        $errorMessage .= "Паролата e задължителна!<br/>";
    }

    return $errorMessage;
}

function validateConfirmPassword()
{
    $errorMessage = "";
    if (isset($_POST['rePassword']) && $_POST['rePassword'] != '') {
        $rePassword = $_POST['rePassword'];
        $password = $_POST['assword'];
        if ($password !== $rePassword && isset($password) && $password != '') {
            $errorMessage .= "Двете пароли не са еднакви<br/>";
        }
    } else {
        $errorMessage .= "Повтарянето на паролата e задължително!<br/>";
    }

    return $errorMessage;
}

function validateUserName()
{
    $errorMessage = "";
    if (isset($_POST['username']) && $_POST['username'] != '') {
        $username = $_POST['username'];
        if (strlen($username) < 3 || strlen($username) > 20) {
            $errorMessage .= "Потребителското име трябва да е между 3 и 20 символа<br/>";
        }
    } else {
        $errorMessage .= "Потребителското име e задължително!<br/>";
    }

    return $errorMessage;
}


?>

<!DOCTYPE html>
<html>

<?php include('../header/nav.php') ?>
<div class="row">
    <div class="col-md-4"></div>
    <div class="col-md-4">
        <h2>Register</h2>
        <?php echo "<div class='error' style='color:red'>$error</div>"; ?>
        <form action="register.php" method="POST">
            <div class="form-group">
                <label for="exampleInputEmail1">Username</label>
                <input type="text" name="username" class="form-control" id="exampleInputEmail1">
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Password</label>
                <input type="password" name="password" class="form-control" id="exampleInputPassword1">
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Confirm Password</label>
                <input type="password" name="rePassword" class="form-control" id="exampleInputPassword1">
            </div>
            <button type="submit" name="register" class="btn btn-primary">Register</button>
        </form>
    </div>

</div>

</body>

</html>