<?php
require_once('../MyDB.php');
session_start();

define("USERNAME", "admin");
define("PASSWORD", "admin");

$conn = DbHelper::GetConnection();
$error = "";

if (isset($_POST['login'])) {
    login();
}

function login()
{
    if (isset($_POST['username']) && isset($_POST['password']) && $_POST['username'] != '' && $_POST['password'] != '') {

        $stm = $GLOBALS["conn"]->prepare("SELECT * FROM USERS WHERE USERNAME=? and PASSWORD=?;");
        $stm->execute(array($_POST['username'], $_POST['password']));
        $rows = $stm->fetchAll(PDO::FETCH_ASSOC);

        if (count($rows)) {
            $_SESSION['username'] = $_POST['username'];
            header('Location: /php/main/tasks.php');
        } else {
            $GLOBALS["error"] = "Невалидно потребителско име или парола!";
        }
    } else {
        $GLOBALS["error"] = "Потребителското име и паролата са задължителни полета!";
    }
}


?>


<!DOCTYPE html>
<html>

<?php require('../header/nav.php') ?>
<div class="row">
    <div class="col-md-4"></div>
    <div class="col-md-4">
        <h2>Login</h2>

        <?php echo "<div class='error' style='color:red'>$error</div>"; ?>
        <form action="login.php" method="POST">
            <div class="form-group">
                <label for="inputUsername">Username</label>
                <input type="text" class="form-control" id="inputUsername" name="username">
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Password</label>
                <input type="password" class="form-control" id="exampleInputPassword1" name="password">
            </div>
            <button type="submit" name="login" class="btn btn-primary">Login</button>
        </form>
    </div>
</div>


</body>

</html>