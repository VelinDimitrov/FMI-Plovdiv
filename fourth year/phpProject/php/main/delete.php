<?php
session_start();

require_once('../MyDB.php');

$conn = DbHelper::GetConnection();
$id = -1;
if (isset($_GET['id'])) {
    $id = $_GET['id'];
}

if (isset($_POST["btnYes"])) {
    $stm = $conn->prepare("DELETE FROM TASKS WHERE TASK_ID = ?");
    $stm->execute(array($id));
    header("Location: tasks.php");
} else if (isset($_POST["btnNo"])) {
    header("Location: tasks.php");
}
?>

<!DOCTYPE html>
<html>

<?php require('../header/nav.php') ?>
<form method="post">
    <input type="hidden" value="<?= $id ?>" />
    Избраната задача ще бъде изтрита. Желаете ли да пордължите?
    <input class="btn btn-success" type="submit" name="btnYes" value="Да" />
    <input class="btn btn-danger" type="submit" name="btnNo" value="Не" />
</form>

</body>

</html>