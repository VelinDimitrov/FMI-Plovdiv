export class Advert {
  id?: number;
  title: string;
  description: string;
  numberOfLikes: number;
  type: string;
  category: string;
  isActive: boolean;
  createdBy: number;
  appliedUsers: number[];
  approvedUser: number;
}
