import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdvertUsersAppliedListComponent } from './components/advert-users-applied-list/advert-users-applied-list.component';
import { AdvertFormComponent } from './components/advert/advert-form/advert-form.component';
import { AdvertListComponent } from './components/advert/advert-list/advert-list.component';
import { LoginComponent } from './components/login/login.component';
import { UserAppliedListComponent } from './components/user-applied-list/user-applied-list.component';
import { UserFormComponent } from './components/user/user-form/user-form.component';
import { AuthGuard } from './guards/auth.guard';
import { NonAuthGuard } from './guards/non-auth.guard';

const routes: Routes = [
  { path: 'adverts', component: AdvertListComponent, canActivate: [AuthGuard] },
  {
    path: 'adverts/create',
    component: AdvertFormComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'adverts/edit/:id',
    component: AdvertFormComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'adverts/:id/users/applied',
    component: AdvertUsersAppliedListComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'users/:id/applied',
    component: UserAppliedListComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'users/edit/:id',
    component: UserFormComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'register',
    component: UserFormComponent,
    canActivate: [NonAuthGuard],
  },
  { path: 'login', component: LoginComponent, canActivate: [NonAuthGuard] },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
