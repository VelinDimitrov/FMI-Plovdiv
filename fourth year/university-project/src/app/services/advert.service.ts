import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { Advert } from '../entities/advert.model';
import { AuthenticationService } from './authentication.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class AdvertService {
  url = 'http://localhost:3000/adverts';

  constructor(
    private http: HttpClient,
    private authService: AuthenticationService
  ) {}

  getAdverts(): Observable<Advert[]> {
    return this.http.get<Advert[]>(this.url);
  }

  getAdvert(id: number): Observable<Advert> {
    const url = `${this.url}/${id}`;

    return this.http.get<Advert>(url);
  }

  createAdvert(advert: Advert): Observable<any> {
    advert.createdBy = this.authService.getLoggedUser().id;
    return this.http.post(this.url, advert);
  }

  updateAdvert(advert: Advert): Observable<any> {
    const url = `${this.url}/${advert.id}`;

    return this.http.put(url, advert);
  }

  deleteAdvert(id: number): Observable<any> {
    const url = `${this.url}/${id}`;

    return this.http.delete(url);
  }

  getUserAppliedAdverts(): Observable<Advert[]> {
    let userId = this.authService.getLoggedUser().id;
    return this.getAdverts().pipe(
      map((stream: Advert[]) =>
        stream.filter((advert) => advert.appliedUsers.includes(userId))
      )
    );
  }

  setInactiveAdvertsOnOrganisationDeletion(userId: number): void {
    this.getAdverts()
      .pipe(
        map((stream) => {
          return stream.filter((advert) => advert.createdBy === userId);
        })
      )
      .subscribe(
        (adverts) => {
          adverts.forEach((advert) => {
            advert.isActive = false;
            advert.createdBy = null;
            this.updateAdvert(advert);
          });
        },
        (error) => {
          console.log(error);
        }
      );
  }
}
