import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { map, take } from 'rxjs/operators';
import { User } from 'src/app/entities/user.model';
import { AdvertService } from 'src/app/services/advert.service';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css'],
})
export class UserFormComponent implements OnInit {
  @ViewChild('form', { static: true }) ngForm: NgForm;
  roles: string[] = ['User', 'Organisation'];

  errorMessage: string;
  user: User;

  constructor(
    private authService: AuthenticationService,
    private router: Router,
    private route: ActivatedRoute,
    private userService: UserService,
    private advertService: AdvertService
  ) {
    this.user = {
      email: '',
      password: '',
      name: '',
      role: '',
      likedAdverts: [],
    };
  }

  ngOnInit(): void {
    let params = this.route.snapshot.params;
    if (params.id) {
      this.getUser(params.id);
    }
  }

  onDeleteClick() {
    if (this.user.role === 'Organisation') {
      this.advertService.setInactiveAdvertsOnOrganisationDeletion(this.user.id);
    }

    this.userService.deleteUser(this.user.id).subscribe(
      () => {
        this.authService.logout();
        this.router.navigate(['/']);
      },
      (error) => {
        console.log(error);
      }
    );
  }

  onSubmit() {
    if (!this.user.id) {
      this.authService
        .getUsers()
        .pipe(
          map((stream: User[]) =>
            stream.find((user) => user.email === this.user.email)
          ),
          take(1)
        )
        .subscribe((response) => {
          if (response) {
            this.errorMessage = 'Email has already been taken';

            return;
          }

          this.authService
            .register(this.user)
            .pipe(take(1))
            .subscribe(() => this.router.navigate(['login']));
        });

      return;
    }

    this.userService
      .updateUser(this.user)
      .pipe(take(1))
      .subscribe(
        () => {
          this.router.navigate(['/']);
        },
        (error) => {
          console.log(error);
        }
      );
  }

  private getUser(id: number): void {
    this.userService
      .getUser(id)
      .pipe(take(1))
      .subscribe((response) => {
        this.user = response;
      });
  }
}
