import { Component, OnInit } from '@angular/core';
import { Advert } from 'src/app/entities/advert.model';
import { User } from 'src/app/entities/user.model';
import { AdvertService } from 'src/app/services/advert.service';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-user-applied-list',
  templateUrl: './user-applied-list.component.html',
  styleUrls: ['./user-applied-list.component.css'],
})
export class UserAppliedListComponent implements OnInit {
  adverts: Advert[];
  loggedUser: User;

  constructor(
    private advertService: AdvertService,
    private authService: AuthenticationService
  ) {}

  ngOnInit(): void {
    this.getAdverts();
    this.loggedUser = this.authService.getLoggedUser();
  }

  private getAdverts(): void {
    this.advertService.getUserAppliedAdverts().subscribe(
      (response) => {
        this.adverts = response;
      },
      (error) => {
        console.log(error);
      }
    );
  }
}
