import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Advert } from 'src/app/entities/advert.model';
import { User } from 'src/app/entities/user.model';
import { AdvertService } from 'src/app/services/advert.service';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-advert-users-applied-list',
  templateUrl: './advert-users-applied-list.component.html',
  styleUrls: ['./advert-users-applied-list.component.css'],
})
export class AdvertUsersAppliedListComponent implements OnInit {
  users: User[];
  advert: Advert;

  constructor(
    private advertService: AdvertService,
    private userService: UserService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.getAdvert();
  }

  onApprove(userId: number) {
    this.advert.approvedUser = userId;
    this.advertService.updateAdvert(this.advert).subscribe(
      () => {
        this.getAdvert();
      },
      (error) => {
        console.log(error);
      }
    );
  }

  private getAdvert(): void {
    let params = this.route.snapshot.params;

    this.advertService.getAdvert(params.id).subscribe(
      (response) => {
        this.advert = response;
        this.userService.getUsersById(response.appliedUsers).subscribe(
          (users) => {
            this.users = users;
          },
          (error) => {
            console.log(error);
          }
        );
      },
      (error) => {
        console.log(error);
      }
    );
  }
}
