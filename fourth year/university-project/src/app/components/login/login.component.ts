import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { take } from 'rxjs/operators';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent {
  @ViewChild('form', { static: true }) ngForm: NgForm;

  email: string;
  password: string;
  errorMessage: string;

  constructor(
    private router: Router,
    private authService: AuthenticationService
  ) {}

  onSubmit() {
    this.authService
      .login(this.email, this.password)
      .pipe(take(1))
      .subscribe((response) => {
        if (!response) {
          this.errorMessage = 'Invalid username or password';

          return;
        }

        this.authService.setLoggedUser(response);
        this.router.navigate(['adverts']);
      });
  }
}
