import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { take } from 'rxjs/operators';
import { Advert } from 'src/app/entities/advert.model';
import { User } from 'src/app/entities/user.model';
import { AdvertService } from 'src/app/services/advert.service';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'advert-list',
  templateUrl: './advert-list.component.html',
  styleUrls: ['./advert-list.component.css'],
})
export class AdvertListComponent implements OnInit {
  adverts: Advert[];
  loggedUser: User;

  destroy$ = new Subject<boolean>();

  constructor(
    private advertService: AdvertService,
    private authService: AuthenticationService,
    private userService: UserService
  ) {
    this.loggedUser = {
      email: '',
      password: '',
      name: '',
      role: '',
      likedAdverts: [],
    };
  }

  ngOnInit(): void {
    this.getAdverts();
    this.loggedUser = this.authService.getLoggedUser();
  }

  onAdvertDelete(advertId: number): void {
    this.advertService.deleteAdvert(advertId).subscribe(
      () => {
        this.getAdverts();
      },
      (error) => {
        console.log(error);
      }
    );
  }

  onAdvertLike(advert: Advert) {
    advert.numberOfLikes += 1;
    this.loggedUser.likedAdverts.push(advert.id);

    this.advertService
      .updateAdvert(advert)
      .pipe(take(1))
      .subscribe(
        () => {
          this.userService
            .updateUser(this.loggedUser)
            .pipe(take(1))
            .subscribe(
              () => {
                this.getAdverts();
              },
              (error) => {
                console.log(error);
              }
            );
        },
        (error) => {
          console.log(error);
        }
      );
  }

  onAdvertApplying(advert: Advert) {
    advert.appliedUsers.push(this.loggedUser.id);
    this.advertService
      .updateAdvert(advert)
      .pipe(take(1))
      .subscribe(
        () => {
          this.getAdverts();
        },
        (error) => {
          console.log(error);
        }
      );
  }

  private getAdverts(): void {
    this.advertService.getAdverts().subscribe(
      (response) => {
        this.adverts = response;
      },
      (error) => {
        console.log(error);
      }
    );
  }
}
