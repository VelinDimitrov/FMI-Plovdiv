import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Advert } from 'src/app/entities/advert.model';
import { User } from 'src/app/entities/user.model';

@Component({
  selector: 'app-advert',
  templateUrl: './advert.component.html',
  styleUrls: ['./advert.component.css'],
})
export class AdvertComponent {
  @Input() advert: Advert;
  @Input() loggedUser: User;

  @Output() advertDeleted = new EventEmitter<number>();
  @Output() advertLiked = new EventEmitter<Advert>();
  @Output() advertApplied = new EventEmitter<Advert>();

  constructor() {}
}
