import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AdvertService } from 'src/app/services/advert.service';
import { Advert } from 'src/app/entities/advert.model';
import { take } from 'rxjs/operators';

@Component({
  selector: 'advert-form',
  templateUrl: './advert-form.component.html',
  styleUrls: ['./advert-form.component.css'],
})
export class AdvertFormComponent implements OnInit {
  @ViewChild('form', { static: true }) ngForm: NgForm;

  categories: string[] = [
    'Office administration',
    'Development',
    'Electronics',
    'Automobile',
    'Sports',
  ];
  advertTypes: string[] = ['Part-time', 'Full-time', 'Remote'];

  advert: Advert;

  constructor(
    private advertService: AdvertService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.advert = {
      title: '',
      description: '',
      type: '',
      category: '',
      numberOfLikes: 0,
      isActive: true,
      createdBy: null,
      appliedUsers: [],
      approvedUser: null,
    };
  }

  ngOnInit() {
    let params = this.route.snapshot.params;
    if (params.id) {
      this.getAdvert(params.id);
    }
  }

  onSubmit() {
    if (!this.advert.id) {
      this.advertService.createAdvert(this.advert).subscribe(
        () => {
          this.router.navigate(['/adverts']);
        },
        (error) => {
          console.log(error);
        }
      );

      return;
    }

    this.advertService
      .updateAdvert(this.advert)
      .pipe(take(1))
      .subscribe(
        () => {
          this.router.navigate(['/adverts']);
        },
        (error) => {
          console.log(error);
        }
      );
  }

  private getAdvert(id: number): void {
    this.advertService
      .getAdvert(id)
      .pipe(take(1))
      .subscribe((response) => {
        this.advert = response;
      });
  }
}
