import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { OrganisationComponent } from './components/organisation/organisation.component';
import { UserComponent } from './components/user/user.component';
import { AdvertComponent } from './components/advert/advert.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserFormComponent } from './components/user/user-form/user-form.component';
import { AdvertFormComponent } from './components/advert/advert-form/advert-form.component';
import { AdvertListComponent } from './components/advert/advert-list/advert-list.component';
import { HttpClientModule } from '@angular/common/http';
import { NavComponent } from './components/nav/nav.component';
import { LoginComponent } from './components/login/login.component';
import { UserAppliedListComponent } from './components/user-applied-list/user-applied-list.component';
import { AdvertUsersAppliedListComponent } from './components/advert-users-applied-list/advert-users-applied-list.component';

@NgModule({
  declarations: [
    AppComponent,
    OrganisationComponent,
    UserComponent,
    AdvertComponent,
    UserFormComponent,
    AdvertFormComponent,
    AdvertListComponent,
    NavComponent,
    LoginComponent,
    UserAppliedListComponent,
    AdvertUsersAppliedListComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
