import React from "react";
import ReactDOM from "react-dom";

export default class StopWatch extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      time: 0,
      isOn: false
    };
  }

  startTimer = () => {
    this.timer = setInterval(function() {
      console.log(this.state);
      this.setState(prevState => ({
        time: prevState.time + 1,
        isOn: true
      }));
    }, 1000);
  };

  stopTimer = () => {
    this.setState({ isOn: false });
    clearInterval(this.timer);
  };

  resetTimer = () => {
    this.setState({ time: 0, isOn: false });
  };

  render() {
    console.log(this.state);
    return (
      <React.Fragment>
        <h2>{this.state.time}</h2>
        {!this.state.isOn && <button onClick={this.startTimer}>start</button>}
        {this.state.isOn && <button onClick={this.stopTimer}>stop</button>}
        {this.state.isOn && <button onClick={this.resetTimer}>reset</button>}
      </React.Fragment>
    );
  }
}
