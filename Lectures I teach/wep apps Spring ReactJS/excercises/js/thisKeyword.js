var person = {
  firstName: "John",
  lastName : "Doe",
  id     : 5566,
  fullName : function() {
    return this.firstName + " " + this.lastName;
  }
};

/*
It has different values depending on where it is used:

    1.In a method, this refers to the owner object.
    2.Alone, this refers to the global object.
    3.In a function, this refers to the global object.
    4.In a function, in strict mode, this is undefined.
    5.In an event, this refers to the element that received the event.
    6.Methods like call(), and apply() can refer this to any object.

*/