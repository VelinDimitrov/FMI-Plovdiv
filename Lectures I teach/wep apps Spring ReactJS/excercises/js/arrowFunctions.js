var elements = [
  'Gosho',
  'Pesho',
  'Toshko',
  'blablabla'
];

elements.map(function(element) { 
  return element.length; 
}); 

// The regular function above can be written as the arrow function below
elements.map((element) => {
  return element.length;
});

// written with even less code
elements.map(element => element.length);